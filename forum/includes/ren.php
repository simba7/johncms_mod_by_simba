<?php

/*
////////////////////////////////////////////////////////////////////////////////
// JohnCMS                Mobile Content Management System                    //
// Project site:          http://johncms.com                                  //
// Support site:          http://gazenwagen.com                               //
////////////////////////////////////////////////////////////////////////////////
// Lead Developer:        Oleg Kasyanov   (AlkatraZ)  alkatraz@gazenwagen.com //
// Development Team:      Eugene Ryabinin (john77)    john77@gazenwagen.com   //
//                        Dmitry Liseenko (FlySelf)   flyself@johncms.com     //
////////////////////////////////////////////////////////////////////////////////
*/

defined('_IN_JOHNCMS') or die('Error: restricted access');

use System\Core\DB as DB;

if ($rights == 3 || $rights >= 6) {
    if (!$id) {
        require('../incfiles/head.php');
        echo functions::display_error($lng['error_wrong_data']);
        require('../incfiles/end.php');
        exit;
    }
    $typ = DB::getInstance()->query("SELECT * FROM `forum` WHERE `id` = '$id'");
    $ms = DB::getInstance()->getAssoc($typ);
    if ($ms[type] != "t") {
        require('../incfiles/head.php');
        echo functions::display_error($lng['error_wrong_data']);
        require('../incfiles/end.php');
        exit;
    }
    if (isset($_POST['submit'])) {
        $nn = isset($_POST['nn']) ? functions::check($_POST['nn']) : false;
        if (!$nn) {
            require('../incfiles/head.php');
            echo functions::display_error($lng_forum['error_topic_name'], '<a href="index.php?act=ren&amp;id=' . $id . '">' . $lng['repeat'] . '</a>');
            require('../incfiles/end.php');
            exit;
        }
        // Проверяем, есть ли тема с таким же названием?
        $pt = DB::getInstance()->query("SELECT * FROM `forum` WHERE `type` = 't' AND `refid` = '" . $ms['refid'] . "' and text='$nn' LIMIT 1");
        if (DB::getInstance()->numRows($pt) != 0) {
            require('../incfiles/head.php');
            echo functions::display_error($lng_forum['error_topic_exists'], '<a href="index.php?act=ren&amp;id=' . $id . '">' . $lng['repeat'] . '</a>');
            require('../incfiles/end.php');
            exit;
        }
        DB::getInstance()->query("update `forum` set  text='" . $nn . "' where id='" . $id . "';");
        header("Location: index.php?id=$id");
    } else {
        /*
        -----------------------------------------------------------------
        Переименовываем тему
        -----------------------------------------------------------------
        */
        require('../incfiles/head.php');
        PageBuffer::getInstance()->setTitle($lng_forum['topic_rename']);
        PageBuffer::getInstance()->addChain($lng_forum['topic_rename'], '');
        ?>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <form action="index.php?act=ren&amp;id=' . $id . '" method="post">
                    <div class="form-group">
                        <label class="control-label" for="tn"><?= $lng_forum['topic_name'] ?></label>
                        <input class="form-control" id="tn" type="text" name="nn" value="<?= $ms['text'] ?>"/>
                    </div>
                    <input type="submit" name="submit" value="<?= $lng['save'] ?>" class="btn btn-success">
                </form>
            </div>
        </div>

        <p><a href="index.php?id=<?= $id ?>"><?= $lng['back'] ?></a></p>

        <?
    }
} else {
    require('../incfiles/head.php');
    echo functions::display_error($lng['access_forbidden']);
}
