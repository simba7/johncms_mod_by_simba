<?php

/**
 * @package     JohnCMS
 * @link        http://johncms.com
 * @copyright   Copyright (C) 2008-2011 JohnCMS Community
 * @license     LICENSE.txt (see attached file)
 * @version     VERSION.txt (see attached file)
 * @author      http://johncms.com/about
 */
use System\Core\DB as DB;
defined('_IN_JOHNCMS') or die('Error: restricted access');
if ($rights == 3 || $rights >= 6) {
    $topic = DB::getInstance()->getCount(DB::getInstance()->query("SELECT COUNT(*) FROM `forum` WHERE `type`='t' AND `id`='$id' AND `edit` != '1'"), 0);
    $topic_vote = DB::getInstance()->getCount(DB::getInstance()->query("SELECT COUNT(*) FROM `cms_forum_vote` WHERE `type`='1' AND `topic`='$id'"), 0);
    require_once('../incfiles/head.php');
    if ($topic_vote != 0 || $topic == 0) {
        echo functions::display_error($lng['error_wrong_data'], '<a href="' . htmlspecialchars(getenv("HTTP_REFERER")) . '">' . $lng['back'] . '</a>');
        require('../incfiles/end.php');
        exit;
    }
    PageBuffer::getInstance()->setTitle($lng_forum['add_vote']);
    PageBuffer::getInstance()->addChain($lng_forum['add_vote'], '');
    if (isset($_POST['submit'])) {
        $vote_name = mb_substr(trim($_POST['name_vote']), 0, 50);
        if (!empty($vote_name) && !empty($_POST[0]) && !empty($_POST[1]) && !empty($_POST['count_vote'])) {
            DB::getInstance()->query("INSERT INTO `cms_forum_vote` SET
                `name`='" . DB::getInstance()->toSql($vote_name) . "',
                `time`='" . time() . "',
                `type` = '1',
                `topic`='$id'
            ");
            DB::getInstance()->query("UPDATE `forum` SET  `realid` = '1'  WHERE `id` = '$id'");
            $vote_count = abs(intval($_POST['count_vote']));
            if ($vote_count > 20)
                $vote_count = 20;
            else if ($vote_count < 2)
                $vote_count = 2;
            for ($vote = 0; $vote < $vote_count; $vote++) {
                $text = mb_substr(trim($_POST[$vote]), 0, 30);
                if (empty($text)) {
                    continue;
                }
                DB::getInstance()->query("INSERT INTO `cms_forum_vote` SET
                    `name`='" . DB::getInstance()->toSql($text) . "',
                    `type` = '2',
                    `topic`='$id'
                ");
            }
            ?>
            <div class="gmenu">
                <?= $lng_forum['voting_added'] ?><br>
                <a href="index.php?id=<?= $id ?>"><?= $lng['continue'] ?></a>
            </div>

            <?
        } else {
            ?>
            <div class="rmenu">
                <?= $lng['error_empty_fields'] ?><br>
                <a href="?act=addvote&amp;id=<?= $id ?>"><?= $lng['repeat'] ?></a>
            </div>
            <?
        }
    } else {
        ?>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <form action="index.php?act=addvote&amp;id=<?= $id ?>" method="post">

                    <div class="form-group">
                        <label for="name_vote"><?= $lng_forum['voting'] ?></label>
                        <input type="text"
                               size="20"
                               maxlength="150"
                               name="name_vote"
                               id="name_vote"
                               class="form-control"
                               value="<?= htmlentities($_POST['name_vote'], ENT_QUOTES, 'UTF-8') ?>"
                            />
                    </div>
                    <?
                    if (isset($_POST['plus'])) {
                        ++$_POST['count_vote'];
                    } elseif (isset($_POST['minus'])) {
                        --$_POST['count_vote'];
                    }
                    if ($_POST['count_vote'] < 2 || empty($_POST['count_vote'])) {
                        $_POST['count_vote'] = 2;
                    } elseif ($_POST['count_vote'] > 20) {
                        $_POST['count_vote'] = 20;
                    }

                    for ($vote = 0; $vote < $_POST['count_vote']; $vote++) {
                        ?>
                        <div class="form-group">
                            <label for="name_vote"><?= $lng_forum['answer'] ?> <?= ($vote + 1) ?> (max. 50)</label>
                            <input type="text"
                                   size="20"
                                   maxlength="150"
                                   name="<?= $vote ?>"
                                   id="name_vote"
                                   class="form-control"
                                   value="<?= htmlentities($_POST[$vote], ENT_QUOTES, 'UTF-8') ?>"
                                />
                        </div>
                        <?
                    }
                    ?>
                    <input type="hidden" name="count_vote" value="<?= abs(intval($_POST['count_vote'])) ?>"/>
                    <? if($_POST['count_vote'] < 20): ?>
                        <input type="submit" name="plus" class="btn btn-default" value="<?= $lng_forum['add_answer'] ?>"/>
                    <? endif; ?>
                    <? if($_POST['count_vote'] > 2): ?>
                        <input type="submit" name="minus" class="btn btn-danger" value="<?= $lng_forum['delete_last'] ?>"/>
                    <? endif; ?>
                    <br>
                    <br>
                    <input type="submit" name="submit" class="btn btn-success" value="<?= $lng['save'] ?>"/>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <a href="index.php?id=<?= $id ?>"><?= $lng['back'] ?></a>
            </div>
        </div>

<?
    }
} else {
    header('location: ../index.php?err');
}
