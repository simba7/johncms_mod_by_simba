<?php defined('_IN_JOHNCMS') or die('Restricted access'); ?>

<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2><?= PageBuffer::getInstance()->showTitle(true); ?></h2>
            <div class="clearfix"></div>
        </div>

        <? core::loadComponent('johnadmin', 'iblock_element_edit', 'default', [
            'iblock' => (isset($_REQUEST['iblock_id']) ? intval($_REQUEST['iblock_id']) : 0),
            'id' => (isset($_REQUEST['id']) ? intval($_REQUEST['id']) : 0)
        ]); ?>
    </div>
</div>






