<?php defined('_IN_JOHNCMS') or die('Error: restricted access'); ?>


<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2><?= PageBuffer::getInstance()->showTitle(true); ?></h2>
            <div class="clearfix"></div>
        </div>
        <div class="buttons">
            <a href="/admin/iblocks/add_element/?iblock=<?= intval($_REQUEST['id']) ?>" class="btn btn-primary">Добавить элемент</a>
        </div>

        <? core::loadComponent('johnadmin', 'iblock_list_elements', '', ['iblock_id' => $_REQUEST['id']]); ?>

    </div>
</div>


