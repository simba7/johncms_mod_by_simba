<?php defined('_IN_JOHNCMS') or die('Restricted access');

$iblock_id = (isset($_REQUEST['id']) ? intval($_REQUEST['id']) : 0);

$props = new System\Blocks\IBProps();
$arr_props = $props->getList([['iblock_id', '=', $iblock_id]]);

?>

<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Свойства инфоблока</h2>
            <div class="clearfix"></div>
        </div>
        <div class="buttons">
            <a href="/admin/settings/iblocks/add_prop/?iblock_id=<?= $iblock_id ?>"
               class="btn btn-primary">Добавить</a>
        </div>
        <div class="col-xs-12">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Название</th>
                    <th>Код</th>
                    <th>Тип</th>
                    <th>Удалить</th>
                </tr>
                </thead>
                <tbody>
                <? if (!empty($arr_props)): ?>
                    <? foreach ($arr_props as $arr_prop): ?>
                        <tr>
                            <th scope="row"><?= $arr_prop['id'] ?></th>
                            <td><?= $arr_prop['name'] ?></td>
                            <td><?= $arr_prop['code'] ?></td>
                            <td><?= $arr_prop['type'] ?></td>
                            <td>
                                <a href="/admin/settings/iblocks/delete_prop/?iblock_id=<?= $iblock_id ?>&id=<?= $arr_prop['id'] ?>">Удалить</a>
                            </td>
                        </tr>
                    <? endforeach; ?>
                <? else: ?>
                    <tr>
                        <td colspan="5">
                            Список пуст
                        </td>
                    </tr>
                <? endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>