<?php
/**
 * Created by PhpStorm.
 * User: Simba
 * Date: 02.05.2016
 * Time: 22:49
 */
define('_IN_JOHNCMS', 1);

require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/theme/gentelella/header.php';

$routeRules = System\Core\Config::getInstance()->load('admin_rule_route');
$requestUri = $_SERVER['REQUEST_URI'];


if (!empty($routeRules)) {
    foreach ($routeRules as $rule) {
        if (preg_match($rule['exp'], $requestUri)) {
            if (!empty($rule['file']) && file_exists($_SERVER['DOCUMENT_ROOT'] . $rule['file'])) {
                PageBuffer::getInstance()->addHeader('HTTP/1.1 200 Ok');
                require_once $_SERVER['DOCUMENT_ROOT'] . $rule['file'];
                require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/theme/gentelella/footer.php';
                die();
            }
        }
    }
}

require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/modules/index.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/theme/gentelella/footer.php';