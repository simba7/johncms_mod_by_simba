<?php
/**
 * Created by PhpStorm.
 * User: Maxim Masalov
 * Date: 22.08.2016
 * Time: 21:52
 * Project: johncms_mod_by_simba
 */

define('_IN_JOHNCMS', 1);
define('START_TIME', microtime(TRUE));

require_once $_SERVER['DOCUMENT_ROOT'] . '/incfiles/core.php';

\System\Core\Users::logout();

header('Location: /admin/login.php');
