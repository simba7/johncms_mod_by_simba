<?php
/**
 * Created by PhpStorm.
 * User: Simba
 * Date: 02.05.2016
 * Time: 22:35
 */

defined('_IN_JOHNCMS') or die('Error: restricted access');
define('START_TIME', microtime(TRUE));
require_once $_SERVER['DOCUMENT_ROOT'] . '/incfiles/core.php';

use \System\Core\Users as User;

// Проверяем авторизован ли пользователь
if(!User::isAuthorized() || !User::adminAccess()) {
    header('Location: /admin/login.php');
    exit;
}

define('ADMIN_TEMPLATE', '/admin/theme/gentelella');

// Set title
PageBuffer::getInstance()->setTitle('Администрирование');

// include css
PageBuffer::getInstance()->addCss(ADMIN_TEMPLATE .'/public/css/bootstrap.min.css');
PageBuffer::getInstance()->addCss(ADMIN_TEMPLATE .'/public/css/font-awesome.min.css');
PageBuffer::getInstance()->addCss(ADMIN_TEMPLATE .'/public/iCheck/skins/flat/green.min.css');
PageBuffer::getInstance()->addCss(ADMIN_TEMPLATE .'/public/css/bootstrap-progressbar-3.3.4.min.css');
PageBuffer::getInstance()->addCss(ADMIN_TEMPLATE .'/public/css/custom.css');
PageBuffer::getInstance()->addCss(ADMIN_TEMPLATE .'/public/select2/css/select2.min.css');
PageBuffer::getInstance()->addCss(ADMIN_TEMPLATE .'/public/datatables.net-bs/css/dataTables.bootstrap.min.css');
PageBuffer::getInstance()->addCss(ADMIN_TEMPLATE .'/public/datatables.net-responsive-bs/css/responsive.bootstrap.min.css');

// include js
PageBuffer::getInstance()->addJs(ADMIN_TEMPLATE . '/public/js/jquery.min.js');
PageBuffer::getInstance()->addJs(ADMIN_TEMPLATE . '/public/js/bootstrap.js');
PageBuffer::getInstance()->addJs(ADMIN_TEMPLATE . '/public/js/bootstrap-progressbar.js');
PageBuffer::getInstance()->addJs(ADMIN_TEMPLATE . '/public/js/icheck.js');
PageBuffer::getInstance()->addJs(ADMIN_TEMPLATE . '/public/js/moment/moment.min.js');
PageBuffer::getInstance()->addJs(ADMIN_TEMPLATE . '/public/js/datepicker/daterangepicker.js');
PageBuffer::getInstance()->addJs(ADMIN_TEMPLATE . '/public/datatables.net/js/jquery.dataTables.min.js');
PageBuffer::getInstance()->addJs(ADMIN_TEMPLATE . '/public/datatables.net-bs/js/dataTables.bootstrap.min.js');
PageBuffer::getInstance()->addJs(ADMIN_TEMPLATE . '/public/datatables.net-responsive/js/dataTables.responsive.min.js');
PageBuffer::getInstance()->addJs(ADMIN_TEMPLATE . '/public/datatables.net-responsive-bs/js/responsive.bootstrap.js');
PageBuffer::getInstance()->addJs(ADMIN_TEMPLATE . '/public/select2/js/select2.full.min.js');
PageBuffer::getInstance()->addJs(ADMIN_TEMPLATE . '/public/ckeditor/ckeditor.js');
PageBuffer::getInstance()->addJs(ADMIN_TEMPLATE . '/public/ckeditor/config.js');

PageBuffer::getInstance()->addJs(ADMIN_TEMPLATE . '/public/js/custom.js');

?>
<!DOCTYPE html>
<html lang="<?= core::$lng_iso ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <? PageBuffer::getInstance()->showMeta() ?>
    <title><? PageBuffer::getInstance()->showTitle(); ?></title>
    <? PageBuffer::getInstance()->showCss() ?>
</head>
<body class="nav-md">

<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="/admin/" class="site_title"><i class="fa fa-paw"></i> <span>JohnCMS</span></a>
                </div>
                <div class="clearfix"></div>
                <!-- menu profile quick info -->
                <div class="profile">
                    <div class="profile_pic">
                        <img src="/images/icon-user-default.png" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                        <span>Привествуем,</span>
                        <h2><?= $login ?></h2>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!-- /menu profile quick info -->
                <br />
                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                    <div class="menu_section">
                        <h3>Навигация</h3>
                        <ul class="nav side-menu">
                            <? core::loadComponent('johnadmin', 'mixed_list_iblocks', 'default', []); ?>

                            <li>
                                <a>
                                    <i class="fa fa-wrench"></i> Настройки
                                    <span class="fa fa-chevron-down"></span>
                                </a>
                                <ul class="nav child_menu">
                                    <li><a href="/admin/settings/iblocks/">Инфоблоки</a></li>
                                </ul>
                            </li>

                        </ul>
                    </div>

                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a href="/admin/logout.php" data-toggle="tooltip" data-placement="top" title="Выход">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">

            <div class="nav_menu">
                <nav class="" role="navigation">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="/images/icon-user-default.png" alt=""><?= $login ?>
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="/admin/logout.php"><i class="fa fa-sign-out pull-right"></i> Выход</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>

        </div>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">
