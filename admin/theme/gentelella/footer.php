<?php defined('_IN_JOHNCMS') or die('Error: restricted access'); ?>
    </div>
    <!-- /page content -->

    <!-- footer content -->
    <footer>
        <div class="pull-right">
            <a href="http://johnmod.symbos.su">JohnCMS mod by Simba</a> |
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
        </div>
        <div class="clearfix"></div>
    </footer>
    <!-- /footer content -->
</div>
</div>

<? PageBuffer::getInstance()->showJs(); ?>

</body>
</html>
<?php
echo PageBuffer::getInstance()->endBuffer();