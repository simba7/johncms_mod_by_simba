<?php
/**
 * Created by PhpStorm.
 * User: Maxim Masalov
 * Date: 22.08.2016
 * Time: 20:44
 * Project: johncms_mod_by_simba
 */

define('_IN_JOHNCMS', 1);
define('START_TIME', microtime(TRUE));

require_once $_SERVER['DOCUMENT_ROOT'] . '/incfiles/core.php';

?><!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Авторизация</title>
    <!-- Bootstrap -->
    <link href="/admin/theme/gentelella/public/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="/admin/theme/gentelella/public/css/font-awesome.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="/admin/theme/gentelella/public/css/custom.css" rel="stylesheet">
</head>

<body style="background:#F7F7F7;">
<div class="">
    <div id="wrapper">
        <? core::loadComponent('johnadmin', 'login'); ?>
    </div>
</div>
</body>
</html>
