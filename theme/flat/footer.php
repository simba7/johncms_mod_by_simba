<?php defined('_IN_JOHNCMS') or die('Restricted access');
/**
 * Created by PhpStorm.
 * User: simba
 * Date: 26.01.2016
 * Time: 19:43
 */


// Рекламный блок сайта
if (!empty($cms_ads[2])) {
    echo '<div class="gmenu">' . $cms_ads[2] . '</div>';
}

echo '<div class="fmenu">';
if (isset($_GET['err']) || $headmod != "mainpage" || ($headmod == 'mainpage' && $act)) {
    //echo '<div><a href=\'' . $set['homeurl'] . '\'>' . functions::image('menu_home.png') . $lng['homepage'] . '</a></div>';
}

// Счетчики каталогов
functions::display_counters();

// Рекламный блок сайта
if (!empty($cms_ads[3])) {
    echo '<br />' . $cms_ads[3];
}

/*
-----------------------------------------------------------------
ВНИМАНИЕ!!!
Данный копирайт нельзя убирать в течение 90 дней с момента установки скриптов
-----------------------------------------------------------------
ATTENTION!!!
The copyright could not be removed within 90 days of installation scripts
-----------------------------------------------------------------
*/

?>
<? if($headmod != 'mainpage'): ?>
</div>
<? endif; ?>
</div>


<footer id="footer" class="midnight-blue">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                &copy; <?= date('Y') ?> <a href="http://johncms.com">JohnCMS</a> - <a href="http://johnmod.symbos.su/">mod by simba</a>
                <br>SQL Запросов: <?= $DB->query_count; ?>, Генерация: <?= round(microtime(true)-START_TIME,3);?>
            </div>
            <div class="col-sm-6">
                <ul class="pull-right">
                    <li>Онлайн: <?= counters::online() ?></li>
                    <li><a id="gototop" class="gototop" href="#"><i class="icon-chevron-up"></i></a></li><!--#gototop-->
                </ul>
            </div>
        </div>
    </div>
</footer>


<?
// Отображаем JS
PageBuffer::getInstance()->showJs();
?>
</body>
</html>