$(function() {
    var wbbOpt = {
        buttons: "bold,italic,underline,strike,bullist,|,video,link,code,quote,fontcolor",
        lang: "ru",
        allButtons: {
            code: {
                transform: {
                    '<code>{SELTEXT}</code>':'[code="php"]{SELTEXT}[/code]'
                }
            },
            bullist: {
                transform: {
                    '<ul>{SELTEXT}</ul>':'[list]{SELTEXT}[/list]',
                    '<li>{SELTEXT}</li>':'[*]{SELTEXT}[\/*]'
                }
            },
        }
    }

    $(".bb_editor").wysibb(wbbOpt);
});
