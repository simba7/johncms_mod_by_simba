<?php

/**
 * @package     JohnCMS
 * @link        http://johncms.com
 * @copyright   Copyright (C) 2008-2011 JohnCMS Community
 * @license     LICENSE.txt (see attached file)
 * @version     VERSION.txt (see attached file)
 * @author      http://johncms.com/about
 */

define('_IN_JOHNCMS', 1);

require('incfiles/core.php');
$textl = $lng['registration'];
$headmod = 'registration';
require('incfiles/head.php');
$lng_reg = core::load_lng('registration');

use System\Core\DB as DB;

if (core::$user_id){
    header('Location: /');
    exit;
}

// Если регистрация закрыта, выводим предупреждение
if (core::$deny_registration || !$set['mod_reg']) {
    echo '<div class="alert alert-dismissible alert-danger">' . $lng_reg['registration_closed'] . '</div>';
    require('incfiles/end.php');
    exit;
}

$captcha = isset($_POST['captcha']) ? trim($_POST['captcha']) : NULL;
$reg_nick = isset($_POST['nick']) ? trim($_POST['nick']) : '';
$lat_nick = functions::rus_lat(mb_strtolower($reg_nick));
$reg_pass = isset($_POST['password']) ? trim($_POST['password']) : '';
$reg_name = isset($_POST['imname']) ? trim($_POST['imname']) : '';
$reg_about = isset($_POST['about']) ? trim($_POST['about']) : '';
$reg_sex = isset($_POST['sex']) ? functions::check(mb_substr(trim($_POST['sex']), 0, 2)) : '';

if (isset($_POST['submit'])) {
    // Принимаем переменные
    $error = array();

    // Проверка Логина
    if (empty($reg_nick)) {
        $error['login'][] = $lng_reg['error_nick_empty'];
    } elseif (mb_strlen($reg_nick) < 2 || mb_strlen($reg_nick) > 15) {
        $error['login'][] = $lng_reg['error_nick_lenght'];
    }

    if (preg_match('/[^\da-z\-\@\*\(\)\?\!\~\_\=\[\]]+/', $lat_nick)) {
        $error['login'][] = $lng['error_wrong_symbols'];
    }

    // Проверка пароля
    if (empty($reg_pass)) {
        $error['password'][] = $lng['error_empty_password'];
    } elseif (mb_strlen($reg_pass) < 3 || mb_strlen($reg_pass) > 10) {
        $error['password'][] = $lng['error_wrong_lenght'];
    }

    if (preg_match('/[^\dA-Za-z]+/', $reg_pass)) {
        $error['password'][] = $lng['error_wrong_symbols'];
    }

    // Проверка пола
    if ($reg_sex != 'm' && $reg_sex != 'zh') {
        $error['sex'] = $lng_reg['error_sex'];
    }

    // Проверка кода CAPTCHA
    if (!$captcha
        || !isset($_SESSION['code'])
        || mb_strlen($captcha) < 4
        || $captcha != $_SESSION['code']
    ) {
        $error['captcha'] = $lng['error_wrong_captcha'];
    }
    unset($_SESSION['code']);

    // Проверка переменных
    if (empty($error)) {
        $pass = md5(md5($reg_pass));
        $reg_name = functions::check(mb_substr($reg_name, 0, 20));
        $reg_about = functions::check(mb_substr($reg_about, 0, 500));
        // Проверка, занят ли ник
        $req = DB::getInstance()->query("SELECT * FROM `users` WHERE `name_lat`='" . DB::getInstance()->toSql($lat_nick) . "'");
        if (DB::getInstance()->numRows($req) != 0) {
            $error['login'][] = $lng_reg['error_nick_occupied'];
        }
    }
    if (empty($error)) {
        $preg = $set['mod_reg'] > 1 ? 1 : 0;
        DB::getInstance()->query("INSERT INTO `users` SET
            `name` = '" . DB::getInstance()->toSql($reg_nick) . "',
            `name_lat` = '" . DB::getInstance()->toSql($lat_nick) . "',
            `password` = '" . DB::getInstance()->toSql($pass) . "',
            `imname` = '$reg_name',
            `about` = '$reg_about',
            `sex` = '$reg_sex',
            `rights` = '0',
            `ip` = '" . core::$ip . "',
            `ip_via_proxy` = '" . core::$ip_via_proxy . "',
            `browser` = '" . DB::getInstance()->toSql($agn) . "',
            `datereg` = '" . time() . "',
            `lastdate` = '" . time() . "',
            `sestime` = '" . time() . "',
            `preg` = '$preg',
            `set_user` = '',
            `set_forum` = '',
            `set_mail` = '',
            `smileys` = ''
        ");
        $usid = DB::getInstance()->lastID();

        // Отправка системного сообщения
        $set_mail = unserialize($set['setting_mail']);

        if (!isset($set_mail['message_include'])) {
            $set_mail['message_include'] = 0;
        }

        if ($set_mail['message_include']) {
            $array = array('{LOGIN}', '{TIME}');
            $array_replace = array($reg_nick, '{TIME=' . time() . '}');

            if (empty($set['them_message'])) {
                $set['them_message'] = $lng_mail['them_message'];
            }

            if (empty($set['reg_message'])) {
                $set['reg_message'] = $lng['hi'] . ", {LOGIN}\r\n" . $lng_mail['pleased_see_you'] . "\r\n" . $lng_mail['come_my_site'] . "\r\n" . $lng_mail['respectfully_yours'];
            }

            $theme = str_replace($array, $array_replace, $set['them_message']);
            $system = str_replace($array, $array_replace, $set['reg_message']);
            DB::getInstance()->query("INSERT INTO `cms_mail` SET
			    `user_id` = '0',
			    `from_id` = '" . $usid . "',
			    `text` = '" . DB::getInstance()->toSql($system) . "',
			    `time` = '" . time() . "',
			    `sys` = '1',
			    `them` = '" . DB::getInstance()->toSql($theme) . "'
			");
        }

        ?>

        <div class="alert alert-dismissible alert-success">
            <strong><?= $lng_reg['you_registered'] ?></strong><br>
            <?= $lng_reg['your_id'] ?>: <b><?= $usid ?></b><br/>
            <?= $lng_reg['your_login'] ?>: <b><?= $reg_nick ?></b><br/>
            <?= $lng_reg['your_password'] ?>: <b><?= $reg_pass ?></b>
            <?
            if ($set['mod_reg'] == 1) {
                echo '<p><b>' . $lng_reg['moderation_note'] . '</b></p>';
            } else {
                $_SESSION['uid'] = $usid;
                $_SESSION['ups'] = md5(md5($reg_pass));
                echo '<p><a href="' . $home . '">' . $lng_reg['enter'] . '</a></p>';
            }
            ?>
        </div>
<?
        require('incfiles/end.php');
        exit;
    }
}

/*
-----------------------------------------------------------------
Форма регистрации
-----------------------------------------------------------------
*/



?>
    <form class="form-horizontal" action="registration.php" method="post">
        <fieldset>
            <legend><?= $lng['registration'] ?></legend>
            <?
            if ($set['mod_reg'] == 1) echo '<div class="alert alert-dismissible alert-warning"><p>' . $lng_reg['moderation_warning'] . '</p></div>';

            if(isset($error) && is_array($error)) {
                ?>
                <div class="alert alert-dismissible alert-danger">
                    <?
                    foreach($error as $err_str) {
                        if(is_array($err_str)){
                            foreach($err_str as $err) {
                                echo $err.'<br>';
                            }
                        } else {
                            echo $err_str.'<br>';
                        }
                    }
                    ?>
                </div>
                <?
            }
            ?>
            <div class="form-group<?= (isset($error['login']) ? ' has-error' : '') ?>">
                <label for="inputLogin" class="col-lg-2 control-label"><?= $lng_reg['login'] ?></label>
                <div class="col-lg-10">
                    <input type="text" name="nick" class="form-control" id="inputLogin" value="<?= htmlspecialchars($reg_nick) ?>" placeholder="<?= $lng_reg['login'] ?>">
                    <span class="help-block">
                        <?= $lng_reg['login_help'] ?>
                    </span>
                </div>
            </div>
            <div class="form-group<?= (isset($error['password']) ? ' has-error' : '') ?>">
                <label for="inputPassword" class="col-lg-2 control-label"><?= $lng_reg['password'] ?></label>
                <div class="col-lg-10">
                    <input type="password" name="password" class="form-control" id="inputPassword" placeholder="<?= $lng_reg['password'] ?>">
                    <span class="help-block">
                        <?= $lng_reg['password_help'] ?>
                    </span>
                </div>
            </div>
            <div class="form-group<?= (isset($error['sex']) ? ' has-error' : '') ?>">
                <label for="select" class="col-lg-2 control-label"><?= $lng_reg['sex'] ?></label>
                <div class="col-lg-10">
                    <select name="sex" class="form-control" id="select">
                        <option value="?">-?-</option>
                        <option value="m"<?= ($reg_sex == 'm' ? ' selected="selected"' : '') ?>><?= $lng_reg['sex_m'] ?></option>
                        <option value="zh"<?= ($reg_sex == 'zh' ? ' selected="selected"' : '') ?>><?= $lng_reg['sex_w'] ?></option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="inputName" class="col-lg-2 control-label"><?= $lng_reg['name'] ?></label>
                <div class="col-lg-10">
                    <input type="text" name="imname" class="form-control" id="inputName" value="<?= htmlspecialchars($reg_name) ?>" placeholder="<?= $lng_reg['name'] ?>">
                    <span class="help-block">
                        <?= $lng_reg['name_help'] ?>
                    </span>
                </div>
            </div>

            <div class="form-group">
                <label for="inputAbout" class="col-lg-2 control-label"><?= $lng_reg['about'] ?></label>
                <div class="col-lg-10">
                    <textarea class="form-control" name="about" rows="3" id="inputAbout"><?= htmlspecialchars($reg_about) ?></textarea>
                    <span class="help-block">
                        <?= $lng_reg['about_help'] ?>
                    </span>
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-2"></div>
                <div class="col-lg-10">
                    <img src="captcha.php?r=<?= rand(1000, 9999) ?>" alt="CAPTCHA">
                </div>
            </div>
            <div class="form-group<?= (isset($error['captcha']) ? ' has-error' : '') ?>">
                <label for="captcha_word" class="col-lg-2 control-label"><?= $lng_reg['captcha'] ?></label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" id="captcha_word" name="captcha" placeholder="<?= $lng_reg['captcha'] ?>" value="" autocomplete="off">
                    <span class="help-block">
                        <?= $lng_reg['captcha_help'] ?>
                    </span>
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-10 col-lg-offset-2">
                    <button type="submit" name="submit" class="btn btn-primary" value="<?= $lng_reg['registration'] ?>"><?= $lng_reg['registration'] ?></button>
                </div>
            </div>
        </fieldset>
    </form>

    <div class="alert alert-dismissible alert-warning">
        <?= $lng_reg['registration_terms'] ?>
    </div>


<?
require('incfiles/end.php');