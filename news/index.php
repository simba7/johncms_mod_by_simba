<?php

/**
 * @package     JohnCMS
 * @link        http://johncms.com
 * @copyright   Copyright (C) 2008-2011 JohnCMS Community
 * @license     LICENSE.txt (see attached file)
 * @version     VERSION.txt (see attached file)
 * @author      http://johncms.com/about
 */

require $_SERVER['DOCUMENT_ROOT'] . '/incfiles/head.php';
PageBuffer::getInstance()->setTitle('Новости');
PageBuffer::getInstance()->addChain('Новости', '/news/');
?>

<? core::loadComponent('johncms', 'iblock_list_elements', '', [
    'iblock_id' => '1'
]); ?>

<? require('../incfiles/end.php'); ?>