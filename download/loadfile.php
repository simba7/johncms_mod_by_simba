<?php
/**
 * @package     JohnCMS
 * @link        http://johncms.com
 * @copyright   Copyright (C) 2008-2011 JohnCMS Community
 * @license     LICENSE.txt (see attached file)
 * @version     VERSION.txt (see attached file)
 * @author      http://johncms.com/about
 */

define('_IN_JOHNCMS', 1);

$headmod = 'download';
use System\Core\DB as DB;
require_once '../incfiles/core.php';
require_once 'functions.php';
$view = functions::check($_GET['down']);
$jad = intval($_GET['jad']);
$file = DB::getInstance()->query("SELECT * FROM `downfiles` WHERE `way` = '" . $view . "'");
$file = DB::getInstance()->getAssoc($file);
$count = $file['count'] + 1;
if($jad == 1){
  $jadf = str_replace('.jar','.jad',$file['way']);
  $load = $jadf;
}else{ $load = $file['way']; }
if ($_SESSION['down'] !== $file['id'])
{
DB::getInstance()->query("UPDATE `downfiles` set `count` = '" . $count . "' WHERE `id` = '".$file['id']."'");
}
$_SESSION['down'] = $file['id'];

header('location: '.$loadroot.'/'.$load);
