<?php
/**
 * @package     JohnCMS
 * @link        http://johncms.com
 * @copyright   Copyright (C) 2008-2011 JohnCMS Community
 * @license     LICENSE.txt (see attached file)
 * @version     VERSION.txt (see attached file)
 * @author      http://johncms.com/about
 *
 * @var $lng_dl
 * @var $lng
 */

define('_IN_JOHNCMS', 1);
$textl = $lng_dl['bookmarks'];
require_once '../incfiles/head.php';
use System\Core\DB as DB;
$lng_dl = core::load_lng('downloads');

if(!$user_id){
    echo'<div class="alert alert-dismissible alert-danger">'.$lng_dl['only_authorized'].'</div>';
    echo'<div class="menu"><a href="index.php">'.$lng['back'].'</a></div>';
    include_once '../incfiles/end.php';
    exit;
}

$id = intval($_GET['id']);
$dejst = $_GET['dejst'];
switch($dejst){
    ///////////////////////////////////////////
    /////////// Создание закладок /////////////
    ///////////////////////////////////////////
    case'add':
        PageBuffer::getInstance()->setTitle($lng_dl['add_bookmark'], true);

    DB::getInstance()->query("INSERT INTO `down_bookmarks` SET `user` = '".$user_id."', `file` = '".$id."', `time` = '".time()."';");
    echo'<div class="alert alert-dismissible alert-success">'.$lng_dl['bookmark_added'].'</div>';
    echo'<div class="menu"><a href="index.php">'.$lng['back'].'</a></div>';
    break;
    ///////////////////////////////////////////
    /////////// Удаление закладок /////////////
    ///////////////////////////////////////////
    case'del':
        PageBuffer::getInstance()->setTitle($lng_dl['del_bookmark'], true);

    DB::getInstance()->query("DELETE FROM `down_bookmarks` WHERE `file` = '".$id."' AND `user` = '".$user_id."'");
    echo'<div class="alert alert-dismissible alert-success">'.$lng_dl['bookmark_deleted'].'</div>';
    echo'<div class="menu"><a href="index.php">'.$lng['back'].'</a></div>';
    break;
    ///////////////////////////////////////////
    /////////// Страница закладок /////////////
    ///////////////////////////////////////////
    default:

        PageBuffer::getInstance()->addChain($lng_dl['bookmarks'], '/download/index.php?act=bookmarks');

    $total = DB::getInstance()->getCount(DB::getInstance()->query("SELECT COUNT(*) FROM `down_bookmarks` WHERE `user` = '".$user_id."'"), 0);
    if($total){
    $zap = DB::getInstance()->query("SELECT * FROM `down_bookmarks` WHERE `user` = '".$user_id."' ORDER BY `time` DESC LIMIT " . $start . "," . $kmess);
    while ($zap2 = DB::getInstance()->getAssoc($zap)) {
    echo ($i % 2) ? '<div class="list1">' : '<div class="list2">';
    $i++;

    $filename = DB::getInstance()->query("SELECT * FROM `downfiles` WHERE `id` = '".$zap2['file']."';");
    $filename = DB::getInstance()->getAssoc($filename);
    $filename = explode('||||', $filename['name']);
    $filtime = date("d.m.Y - H:i", $zap2['time']);
    echo $lng_dl['file'].': <a href="file_'.$zap2['file'].'.html">'.$filename[0].'</a><div class="sub">'.$lng_dl['added'].' '.$filtime.'<br/>';
    echo'<a href="index.php?act=bookmarks&amp;dejst=del&amp;id='.$zap2['file'].'">'.$lng_dl['del_bookmark'].'</a></div>';
    echo'</div>';
    }
    echo'<div class="phdr">'.$lng['total'].': '.$total.'</div>';
    if ($total > $kmess)
    {
    	echo functions::display_pagination('index.php?act=bookmarks&amp;', $start, $total, $kmess);
    }
    echo '<div class="menu"><a href="index.php">'.$lng['back'].'</a></div>';
    }
    else
    {
        ?>
        <div class="alert alert-dismissible alert-warning">
            <p>
                <?= $lng['list_empty'] ?><br>

            </p>
        </div>
        <p><a href="/download/"><?= $lng['back'] ?></a></p>
        <?php
    }
    break;
    }
