<?php defined('_IN_JOHNCMS') or die('Error: restricted access');
    /**
     * @package     JohnCMS
     * @link        http://johncms.com
     * @copyright   Copyright (C) 2008-2011 JohnCMS Community
     * @license     LICENSE.txt (see attached file)
     * @version     VERSION.txt (see attached file)
     * @author      http://johncms.com/about
     *
     * @var $lng
     * @var $lng_dl
     */

    $cat = intval($_GET['cat']);

    PageBuffer::getInstance()->setTitle($lng_dl['create_section'] . ' - ' . $lng_dl['downloads'] . ' / ' . $lng_dl['admin_panel']);
    PageBuffer::getInstance()->setTitle($lng_dl['create_section'], true);

    PageBuffer::getInstance()->addChain($lng_dl['downloads'], '/download/');
    PageBuffer::getInstance()->addChain($lng_dl['admin_panel'], '/download/admin.php');
    PageBuffer::getInstance()->addChain($lng_dl['structure_manage'], '/download/admin.php?act=folder');

?>

<?php if (isset($_POST['submit'])): ?>
    <?php
    $section = new DownSection();
    $save_dir = $section->add(
        array(
            'NAME'              => $_POST['name'],
            'FS_NAME'           => $_POST['fs_name'],
            'DESCRIPTION'       => $_POST['description'],
            'FILES_TYPES'       => $_POST['file_types'],
            'USER_UPLOAD'       => $_POST['user_add'],
            'PARENT_SECTION_ID' => $cat,
        )
    );
    ?>
    <?php if (!$save_dir): ?>
        <div class="alert alert-dismissible alert-danger">
            <p>
                <b><?= $lng['error'] ?></b><br>
                <?php foreach ($section->last_error as $error): ?>
                    <?= $error ?><br>
                <?php endforeach; ?>
                <a href="/download/admin.php?act=createdir&amp;cat=<?= $cat ?>"><?= $lng_dl['repeat'] ?></a>
            </p>
        </div>
    <?php else: ?>
        <div class="alert alert-dismissible alert-success">
            <?= $lng_dl['section_created_success'] ?>
        </div>
    <?php endif; ?>
<?php else: ?>
    <form action="/download/admin.php?act=createdir&amp;cat=<?= $cat ?>" method="post">
        <div class="gmenu">
            <div class="form-group">
                <label for="name"><?= $lng_dl['name'] ?></label><br>
                <input class="form-control" type="text" name="name" id="name" value=""/>
            </div>
            <div class="form-group">
                <label for="fs_name"><?= $lng_dl['name_in_file_system'] ?></label><br>
                <input class="form-control" type="text" name="fs_name" id="fs_name" value=""/>
            </div>
            <div class="form-group">
                <label for="description"><?= $lng_dl['description'] ?></label><br>
                <input class="form-control" type="text" name="description" id="description" value=""/>
            </div>
            <div class="form-group">
                <label for="file_types"><?= $lng_dl['file_types'] ?></label><br>
                <input class="form-control" type="text" name="file_types" id="file_types" value=""/>
                <span class="help-block"><?= $lng_dl['file_types_notice'] ?></span>
            </div>
            <div class="form-group">
                <input type="checkbox" id="user_add" name="user_add" value="1"/>
                <label for="user_add"><?= $lng_dl['allow_user_add_files'] ?></label>
            </div>
            <div class="form-group">
                <input class="btn btn-success" type='submit' name='submit' value='<?= $lng['save'] ?>'/>
            </div>
        </div>
    </form>
<?php endif; ?>
<p>
    <a href="/download/admin.php?act=folder"><?= $lng_dl['structure_manage'] ?></a><br>
    <a href="/download/admin.php"><?= $lng_dl['admin_panel'] ?></a>
</p>
