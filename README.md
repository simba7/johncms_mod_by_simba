# [JohnCMS mod by Simba 2.0.1](http://johnmod.symbos.su)

[![SemVer](http://img.shields.io/badge/semver-✓-brightgreen.svg?style=plastic)](http://semver.org)
[![License](https://img.shields.io/badge/license-GPL%20v.3-blue.svg?style=plastic)](https://www.gnu.org/licenses/gpl-3.0-standalone.html)

JohnCMS Content Management System is for the construction of the sites that will be looked through from mobiles.

## The main possibilities of the system:
- multilingual, the possibility to install / delete the languages of the interface.
- a high level of security
- quick in work, built on MySQL
- advanced system of differentiation of rights for administrators/moderators
- forum with the possibility to open / close themes, to create voting, possibility to 
  attach files to the theme and etc.
- private photo albums
- private guest books
- advanced library with unlimited nesting of sections and the possibility for the guests to publish their own articles.
  There is a moderation of the articles, that had been published by guests.
  The automatic compillation of Java books.
- photo gallery
- download centre with unlimited nesting of sections, counter, raiting and comments
- private mail with the possibility to attach files
- easy work with smiles
- change of styles
- and many other things...

## System Requirements
- PHP version is not low than 5.4
- MySQL version is not low than 5.1
- .htaccess support

## Installation
1. Download script http://johnmod.symbos.su/johncmssetup.zip
2. Unpack to base dir
3. Run script http://your.site/johncmssetup.php

---

# [JohnCMS mod by Simba 2.0.1](http://johnmod.symbos.su)

Система управления сайтом JohnCMS предназначена для построения сайтов, которые будут просматриваться с мобильных телефонов.

## Основные возможности системы:
- мультиязычность, возможность устанавливать / удалять языки интерфейса
- высокий уровень безопасности
- быстрая в работе, построена на MySQL
- продвинутая система разграничений прав для админов/модеров
- форум с возможностью закрепления/закрытия тем, созданием голосований,
  возможностью прикрепления файлов в теме и т.д...
- личные Фотоальбомы
- личные Гостевые книги
- библиотека с неограниченной вложенностью разделов и возможностью для посетителей сайта публиковать свои статьи. Модерация статей, компиляция Java книг.
- фотогалерея
- загруз центр с неограниченной вложенностью разделов, счетчиком, рейтингом и комментариями.
- приват (личная почта) с возможностью прикрепления файлов
- удобная работа со смайлами
- смена стилей
- и многое другое...

## Системные требования
- Версия PHP не ниже 5.4
- MySQL версии не ниже 5.1
- Поддержка .htaccess

## Установка
1. Скачайте установочный скрипт по адресу http://johnmod.symbos.su/johncmssetup.zip
2. Извлеките из архива и поместите в корневую директорию сайта.
3. Перейдите по адресу сайт.ру/johncmssetup.php и следуйте инструкциям мастера установки.