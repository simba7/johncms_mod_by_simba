<?php defined('_IN_JOHNCMS') or die('Restricted access');
/**
 * @var $this Component
 */

// $this->params - Содержит параметры компонента (массив)
$result = [];

// Сообщения об ощибках
$result['errors'] = [];

PageBuffer::getInstance()->setTitle('Добавить тип инфоблоков');

$result['fields'] = [
    [
        'type'        => 'text',
        'name'        => 'name',
        'label'       => 'Название типа инфоблоков',
        'placeholder' => 'Введите название',
        'help'        => '',
        'value'       => (isset($_POST['name']) ? htmlspecialchars($_POST['name']) : ''),
        'required'    => true,
    ],
    [
        'type'        => 'text',
        'name'        => 'code',
        'label'       => 'Код типа инфоблока',
        'placeholder' => 'Введите код типа инфоблока',
        'help'        => 'Разрешены только латинские буквы, цифры и знак нижнего подчеркивания',
        'value'       => (isset($_POST['code']) ? htmlspecialchars($_POST['code']) : ''),
        'required'    => true,
    ]
];

if (isset($_POST['submit']) && $_POST['submit'] == 'save') {
    $blockRes = new System\Blocks\BlockTypes();

    // Добавляем тип инфоблока
    $added = $blockRes->add(['name' => $_POST['name'], 'code' => $_POST['code']]);

    // Если добавлен, возвращаемся в список
    if($added) {
        header('Location: /admin/settings/iblocks/');
        exit;
    } else {
        // Если добавить не удалось, возвращаем ошибки
        $result['errors'] = $blockRes->last_errors;
    }
}


// Массив который попадает в шаблон
$this->result = $result;

// Загружаем языки
$this->loadLanguage();

// Загружаем шаблон
$this->loadTemplate();
