<?php defined('_IN_JOHNCMS') or die('Restricted access');
/**
 * @var $this Component
 */

// $this->params - Содержит параметры компонента (массив)
$result = [];

$this->params['iblock_id'] = (isset($_REQUEST['iblock_id']) ? intval($_REQUEST['iblock_id']) : 0);

// Сообщения об ощибках
$result['errors'] = [];

PageBuffer::getInstance()->setTitle('Добавить свойство');

$result['fields'] = [
    [
        'type'        => 'text',
        'name'        => 'name',
        'label'       => 'Название свойства инфоблоков',
        'placeholder' => 'Введите название',
        'help'        => '',
        'value'       => (isset($_POST['name']) ? htmlspecialchars($_POST['name']) : ''),
        'required'    => true,
    ],
    [
        'type'        => 'text',
        'name'        => 'code',
        'label'       => 'Код свойства инфоблока',
        'placeholder' => 'Введите код свойства инфоблока',
        'help'        => 'Разрешены только латинские буквы, цифры и знак нижнего подчеркивания',
        'value'       => (isset($_POST['code']) ? htmlspecialchars($_POST['code']) : ''),
        'required'    => true,
    ],
    [
        'type'        => 'select',
        'name'        => 'type',
        'label'       => 'Тип свойства',
        'placeholder' => 'Выберите из списка',
        'help'        => '',
        'value'       => (isset($_POST['type']) ? htmlspecialchars($_POST['type']) : ''),
        'multiple'    => false,
        'required'    => true,
        'options'     => [
            'int'    => 'Число',
            'string' => 'Строка',
            'text'   => 'Текст'
        ],
    ]
];

if (isset($_POST['submit']) && $_POST['submit'] == 'save') {

    $ibProps = new System\Blocks\IBProps();

    $added = $ibProps->add($this->params['iblock_id'], [
        'name'     => $_POST['name'],
        'code'     => $_POST['code'],
        'type'     => $_POST['type'],
        'multiple' => 'n',
    ]);

    // Если свойство добавлено, возвращаемся в список
    if ($added) {
        header('Location: /admin/settings/iblocks/props/?id=' . $this->params['iblock_id']);
        exit;
    } else {
        // Если добавить не удалось, возвращаем ошибки
        $result['errors'] = $ibProps->last_errors;
    }
}


// Массив который попадает в шаблон
$this->result = $result;

// Загружаем языки
$this->loadLanguage();

// Загружаем шаблон
$this->loadTemplate();
