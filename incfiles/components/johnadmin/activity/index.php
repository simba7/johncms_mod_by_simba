<?php defined('_IN_JOHNCMS') or die('Restricted access');
/**
 * @var $this Component
 */

$DB = System\Core\DB::getInstance();

// $this->params - Содержит параметры компонента (массив)
$result = [];

// Пользователи
$all_users = System\Core\Users::countUsers();
$last_users = System\Core\Users::countUsers([['datereg', '>', (time() - 86400)]]);

// Гостевая
$count_guestbook = $DB->getCount($DB->query("SELECT COUNT(*) FROM `guest` WHERE `adm`='0'"), 0);

// Библиотека
$lib_mod = $DB->getCount($DB->query("SELECT COUNT(*) FROM `library_texts` WHERE `premod` = '0'"), 0);
$lib_count = $DB->getCount($DB->query("SELECT COUNT(*) FROM `library_texts` WHERE `premod` = '1'"), 0);

// Форум
$req = $DB->query("SELECT COUNT(*) FROM `forum`
                LEFT JOIN `cms_forum_rdm` ON `forum`.`id` = `cms_forum_rdm`.`topic_id` AND `cms_forum_rdm`.`user_id` = '" . core::$user_id . "'
                WHERE `forum`.`type`='t'" . (core::$user_rights >= 7 ? "" : " AND `forum`.`close` != '1'") . "
                AND (`cms_forum_rdm`.`topic_id` IS NULL
                OR `forum`.`time` > `cms_forum_rdm`.`time`)");
$total_unread = $DB->getCount($req, 0);

$msg_forum = $DB->getCount($DB->query("SELECT COUNT(*) FROM `forum` WHERE `type` = 'm' AND `close` != '1'"), 0);


$result[] = [
    'name' => 'Всего пользователей',
    'total' => $all_users,
    'last' => $last_users,
    'last_suffix' => 'За последние сутки',
    'icon' => 'fa-user'
];

$result[] = [
    'name' => 'Сообщений в гостевой',
    'total' => $count_guestbook,
    'last' => counters::guestbook(1),
    'last_suffix' => 'За последние сутки',
    'icon' => 'fa-user'
];

$result[] = [
    'name' => 'Статей в библиотеке',
    'total' => $lib_count,
    'last' => $lib_mod,
    'last_suffix' => 'на модерации',
    'icon' => 'fa-user'
];

$result[] = [
    'name' => 'Сообщений на форуме',
    'total' => $msg_forum,
    'last' => $total_unread,
    'last_suffix' => 'Непрочитанных',
    'icon' => 'fa-clock-o'
];


// Массив который попадает в шаблон
$this->result = $result;

// Загружаем языки
$this->loadLanguage();

// Загружаем шаблон
$this->loadTemplate();
