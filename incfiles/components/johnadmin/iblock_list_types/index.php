<?php defined('_IN_JOHNCMS') or die('Restricted access');
/**
 * @var $this Component
 */

// $this->params - Содержит параметры компонента (массив)
$result = [];
$list = [];

PageBuffer::getInstance()->setTitle('Типы инфоблоков');

// Если ajax запрос, собираем массив для datatables
if (isset($_REQUEST['is_ajax'])) {
    // Очищаем буфер вывода
    \PageBuffer::getInstance()->cleanBuffer();

    // Сдвиги выборки для постраничной навигации
    $offset = (!empty($_REQUEST['start']) ? intval($_REQUEST['start']) : 0);
    $limit = (!empty($_REQUEST['length']) ? intval($_REQUEST['length']) : 0);

    // Получаем список элементов инфоблока
    $blockRes = new System\Blocks\BlockTypes();
    $list = $blockRes->getList('', [$offset => $limit]);

    // Готовим дополнительные поля для вывода
    foreach ($list as $key=>$item) {
        $list[$key]['name_link'] = '<a href="/admin/settings/iblocks/list/?type='.$item['id'].'">'.$item['name'].'</a>';
    }


    // Собираем массив данных понятный для jquery datatables
    $draw = isset($_REQUEST['draw']) ? intval($_REQUEST['draw']) : 1;
    $return_array = [
        "draw"            => $draw,
        "recordsTotal"    => $blockRes->last_count,
        "recordsFiltered" => $blockRes->last_count,
        "data"            => $list
    ];

    echo json_encode($return_array);

    exit();
}


$result['columns'] = [
    'id'   => 'ID',
    'name_link' => 'Название',
    'code' => 'Код',
];


$result['items'] = $list;

// Массив который попадает в шаблон
$this->result = $result;

// Загружаем языки
$this->loadLanguage();

// Загружаем шаблон
$this->loadTemplate();
