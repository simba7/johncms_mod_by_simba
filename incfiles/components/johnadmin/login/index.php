<?php defined('_IN_JOHNCMS') or die('Restricted access');
/**
 * @var $this Component
 */

// $this->params - Содержит параметры компонента (массив)
$result = [];

use \System\Core\Users as User;

// Проверяем авторизован ли пользователь
if (User::isAuthorized()) {
    header('Location: /admin/');
    exit;
}

if (!empty($_POST['login']) && !empty($_POST['password'])) {

    if (User::authorize($_POST['login'], $_POST['password'], true)) {
        header('Location: /admin/');
        exit;
    } else {
        $result['error'] = 'Неверный логин или пароль!';
    }

} else {
    if(!empty($_POST['submit'])) {
        $result['error'] = 'Введите логин и пароль!';
    }
}

// Массив который попадает в шаблон
$this->result = $result;

// Загружаем языки
$this->loadLanguage();

// Загружаем шаблон
$this->loadTemplate();
