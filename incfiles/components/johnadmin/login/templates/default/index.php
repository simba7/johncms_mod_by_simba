<?php defined('_IN_JOHNCMS') or die('Error: restricted access');
/**
 * @var $this Component
 */
?>

<div id="login" class="form">
    <section class="login_content">
        <form name="login" action="/admin/login.php" method="post">
            <h1>Авторизация</h1>
            <? if(!empty($this->result['error'])): ?>
                <div class="alert alert-danger"><?= $this->result['error'] ?></div>
            <? endif; ?>
            <div>
                <input type="text" name="login" class="form-control" placeholder="Логин" required=""/>
            </div>
            <div>
                <input type="password" name="password" class="form-control" placeholder="Пароль" required=""/>
            </div>
            <div>
                <button name="submit" value="submit" class="btn btn-default submit">Войти</button>
            </div>
            <div class="clearfix"></div>
            <div class="separator">
                <div class="clearfix"></div>
                <br/>
                <div>
                    <h2><i class="fa fa-paw" style="font-size: 26px;"></i> JohnCMS</h2>
                    <p>©2015 JohnCMS mod by Simba</p>
                </div>
            </div>
        </form>
    </section>
</div>