<?php defined('_IN_JOHNCMS') or die('Restricted access');
/**
 * @var $this Component
 */

// $this->params - Содержит параметры компонента (массив)
$result = [];

// Сообщения об ощибках
$result['errors'] = [];

$result['iblock'] = intval($this->params['iblock']);

if ($result['iblock'] < 1) {
    die('Не задан ID инфоблока');
}

PageBuffer::getInstance()->setTitle('Добавить элемент');

$result['fields'] = [];

// Сопоставлем типы полей инфоблока с типами полей которые понимает компонент форм.
$prop_types = [
    'int'    => 'number',
    'string' => 'text',
    'text'   => 'textarea',
];

$props = new System\Blocks\IBProps();
$arr_props = $props->getList([['iblock_id', '=', $result['iblock']]]);

// Собираем массив с описанием полей
foreach ($arr_props as $prop) {
    $result['fields'][] = [
        'type'        => $prop_types[$prop['type']],
        'name'        => 'f_' . $prop['code'],
        'label'       => $prop['name'],
        'placeholder' => $prop['name'],
        'help'        => '',
        'value'       => (isset($_POST['f_' . $prop['code']]) ? htmlspecialchars($_POST['f_' . $prop['code']]) : ''),
        'required'    => true,
        'editor'      => true
    ];
}

// Сохраняем данные формы
if (isset($_POST['submit']) && $_POST['submit'] == 'save') {

    // Создаем объект инфоблока
    $blockRes = new System\Blocks\Elements($result['iblock']);

    // Если не удалось получить данные по инфоблоку, то редиректим на страницу инфоблока
    if (empty($blockRes->iblock_data)) {
        header('Location: /admin/iblocks/index.php?id=' . $result['iblock']);
        exit;
    }

    // Добавляем элемент в инфоблок
    $added = $blockRes->add($_POST);

    // Если добавлен, возвращаемся в список
    if ($added > 0) {
        header('Location: /admin/iblocks/index.php?id=' . $result['iblock']);
        exit;
    } else {
        // Если добавить не удалось, возвращаем ошибки
        $result['errors'][] = 'Ошибка добавления объекта';
    }
}


// Массив который попадает в шаблон
$this->result = $result;

// Загружаем языки
$this->loadLanguage();

// Загружаем шаблон
$this->loadTemplate();
