<?php defined('_IN_JOHNCMS') or die('Restricted access');
/**
 * @var $this Component
 */

// $this->params - Содержит параметры компонента (массив)
$result = [];

if (empty($this->params['iblock']) && intval($this->params['iblock']) < 1) {
    exit('Не задан обязательный параметр компонента "iblock" - ID инфоблока');
}

if (empty($this->params['id']) && intval($this->params['id']) < 1) {
    exit('Не задан обязательный параметр компонента "id" - ID элемента');
}

// Сообщения об ощибках
$result['errors'] = [];

PageBuffer::getInstance()->setTitle('Изменить элемент');

// Формируем массив полей
$result['fields'] = [];

// Сопоставлем типы полей инфоблока с типами полей которые понимает компонент форм.
$prop_types = [
    'int'    => 'number',
    'string' => 'text',
    'text'   => 'textarea',
];

$props = new System\Blocks\IBProps();
$arr_props = $props->getList([['iblock_id', '=', $this->params['iblock']]]);

$ibElements = new System\Blocks\Elements($this->params['iblock']);
$arrElement = $ibElements->getByID($this->params['id']);


// Собираем массив с описанием полей
foreach ($arr_props as $prop) {
    $result['fields'][] = [
        'type'        => $prop_types[$prop['type']],
        'name'        => 'f_' . $prop['code'],
        'label'       => $prop['name'],
        'placeholder' => $prop['name'],
        'help'        => '',
        'value'       => (isset($_POST['f_' . $prop['code']]) ? htmlspecialchars($_POST['f_' . $prop['code']]) : htmlspecialchars($arrElement['f_' . $prop['code']])),
        'required'    => true,
        'editor'      => true
    ];
}

if (isset($_POST['submit']) && $_POST['submit'] == 'save') {

    $update_fields = [];
    foreach ($result['fields'] as $field) {
        $update_fields[$field['name']] = (isset($_POST[$field['name']]) ? $_POST[$field['name']] : '');
    }

    // Обновляем данные
    $added = $ibElements->update($this->params['id'], $update_fields);

    // Если обновлен, возвращаемся в список
    if ($added) {
        header('Location: /admin/iblocks/index.php?id=' . $this->params['iblock']);
        exit;
    } else {
        // Если обновить не удалось, возвращаем ошибки
        $result['errors'] = $blockRes->last_errors;
    }
}

// Массив который попадает в шаблон
$this->result = $result;

// Загружаем языки
$this->loadLanguage();

// Загружаем шаблон
$this->loadTemplate();
