<?php defined('_IN_JOHNCMS') or die('Restricted access');
/**
 * @var $this Component
 */

// $this->params - Содержит параметры компонента (массив)
$result = [];

// Получаем список типов инфоблоков
$blockRes = new System\Blocks\BlockTypes();
$list = $blockRes->getList();

$types_list = [];
foreach ($list as $item) {
    $types_list[$item['id']] = $item;
}

// Получаем список инфоблоков
$iblocksRes = new System\Blocks\IBlocks();
$listIblocks = $iblocksRes->getList();

// Распределяем инфоблоки в общий массив по типам
foreach ($listIblocks as $index => $listIblock) {
    $listIblock['url'] = '/admin/iblocks/index.php?id=' . $listIblock['id'];
    $types_list[$listIblock['type']]['iblocks'][] = $listIblock;
}

$result = $types_list;

// Массив который попадает в шаблон
$this->result = $result;

// Загружаем языки
$this->loadLanguage();

// Загружаем шаблон
$this->loadTemplate();
