<?php defined('_IN_JOHNCMS') or die('Error: restricted access');
/**
 * @var $this Component
 */
?>

<? if(!empty($this->result)): ?>
<li>
    <a>
        <i class="fa fa-sitemap"></i> Инфоблоки
        <span class="fa fa-chevron-down"></span>
    </a>
    <ul class="nav child_menu">
        <? foreach ($this->result as $item): ?>
            <? if(!empty($item['iblocks'])): ?>
                <li><a><?= $item['name'] ?><span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <? foreach ($item['iblocks'] as $iblock): ?>
                            <li class="sub_menu">
                                <a href="<?= $iblock['url'] ?>"><?= $iblock['name'] ?></a>
                            </li>
                        <? endforeach; ?>
                    </ul>
                </li>
            <? else: ?>
                <li><a href="#<?= $item['code'] ?>"><?= $item['name'] ?></a></li>
            <? endif; ?>
        <? endforeach; ?>
    </ul>
</li>
<? endif; ?>
