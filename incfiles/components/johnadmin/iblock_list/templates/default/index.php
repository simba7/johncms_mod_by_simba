<?php defined('_IN_JOHNCMS') or die('Error: restricted access');
/**
 * @var $this Component
 */
?>

<? if (!empty($this->result) && !empty($this->result['columns'])): ?>

    <script type="text/javascript">
        window.onload = function(e) {
            var table = $('#list').DataTable({
                "responsive": true,
                "processing": true,
                "autoWidth": true,
                "searching": false,
                "language": {
                    url: '<?= ADMIN_TEMPLATE ?>/public/datatables.net/js/Russian.json'
                },
                "serverSide": true,
                "ajax": "/admin/settings/iblocks/list/?type=<?= $this->result['type'] ?>&is_ajax=1",
                columns: [
                    <? foreach ($this->result['columns'] as $key=>$column): ?>
                    {data: '<?= $key ?>', "orderable": false},
                    <? endforeach; ?>
                ],
                order: [[0, 'asc']]
            });
        };
    </script>

    <table id="list" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <? foreach ($this->result['columns'] as $column): ?>
                <th><?= $column ?></th>
            <? endforeach; ?>
        </tr>
        </thead>
    </table>


<? endif; ?>
