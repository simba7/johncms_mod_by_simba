<?php defined('_IN_JOHNCMS') or die('Restricted access');
/**
 * @var $this Component
 */

// $this->params - Содержит параметры компонента (массив)
$result = [];
$list = [];

$result['type'] = intval($_REQUEST['type']);

PageBuffer::getInstance()->setTitle('Типы инфоблоков');

// Если ajax запрос, собираем массив для datatables
if (isset($_REQUEST['is_ajax'])) {
    // Очищаем буфер вывода
    \PageBuffer::getInstance()->cleanBuffer();

    // Сдвиги выборки для постраничной навигации
    $offset = (!empty($_REQUEST['start']) ? intval($_REQUEST['start']) : 0);
    $limit = (!empty($_REQUEST['length']) ? intval($_REQUEST['length']) : 0);

    // Получаем список элементов инфоблока
    $blocksRes = new System\Blocks\IBlocks();
    $list = $blocksRes->getList([['type', '=', $result['type']]], [$offset => $limit]);

    // Готовим дополнительные поля для вывода
    foreach ($list as $key => $item) {
        $list[$key]['name_link'] = '<a href="/admin/settings/iblocks/edit/?id=' . $item['id'] . '">' . $item['name'] . '</a>';
        $list[$key]['actions'] = '<div class="x_content">
                <button data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button" aria-expanded="true">
                <i class="fa fa-list"></i> <span class="caret"></span>
                </button>
                <ul role="menu" class="dropdown-menu">
                  <li><a href="/admin/settings/iblocks/edit/?id=' . $item['id'] . '"><i class="fa fa-edit"></i> Изменить</a></li>
                  <li><a href="/admin/settings/iblocks/props/?id=' . $item['id'] . '"><i class="fa fa-cogs"></i> Свойства</a></li>
                  <li class="divider"></li>
                  <li><a href="/admin/settings/iblocks/delete/?type='.$item['type'].'&id=' . $item['id'] . '"><i class="fa fa-remove"></i> Удалить</a></li>
                </ul></div>';
    }

    // Собираем массив данных понятный для jquery datatables
    $draw = isset($_REQUEST['draw']) ? intval($_REQUEST['draw']) : 1;
    $return_array = [
        "draw"            => $draw,
        "recordsTotal"    => $blocksRes->count_result,
        "recordsFiltered" => $blocksRes->count_result,
        "data"            => $list
    ];

    echo json_encode($return_array);

    exit();
}


$result['columns'] = [
    'id'        => 'ID',
    'name_link' => 'Название',
    'code'      => 'Код',
    'actions'   => 'Действия',
];


$result['items'] = $list;

// Массив который попадает в шаблон
$this->result = $result;

// Загружаем языки
$this->loadLanguage();

// Загружаем шаблон
$this->loadTemplate();
