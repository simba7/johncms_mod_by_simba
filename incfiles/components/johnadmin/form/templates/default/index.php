<?php defined('_IN_JOHNCMS') or die('Error: restricted access');
/**
 * @var $this Component
 */
?>

<? if (!empty($this->result['fields'])): ?>

    <form action="<?= $this->result['action'] ?>" data-parsley-validate class="form-horizontal form-label-left" method="post" enctype="multipart/form-data">

        <? if(!empty($this->result['errors'])): ?>
            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <? foreach ($this->result['errors'] as $key => $val): ?>
                    <?= $val ?><br>
                <? endforeach; ?>
            </div>
        <? endif; ?>

        <? foreach ($this->result['fields'] as $key => $field): ?>
            <?
            switch ($field['type']):
                case 'text':
                    ?>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="f_<?= $key ?>">
                            <?= $field['label'] ?> <? if ($field['required']): ?><span
                                class="required">*</span><? endif; ?>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="f_<?= $key ?>"
                                   name="<?= $field['name'] ?>"
                                   class="form-control col-md-7 col-xs-12"
                                   value="<?= $field['value'] ?>"
                                <?= (!empty($field['placeholder']) ? 'placeholder="' . $field['placeholder'] . '"' : '') ?>
                                <?= ($field['required'] ? 'required="required"' : '') ?>
                            >
                            <? if (!empty($field['help'])): ?>
                                <span class="help-block"><?= $field['help'] ?></span>
                            <? endif; ?>
                        </div>
                    </div>
                    <?
                    break;
                case 'number':
                    ?>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="f_<?= $key ?>">
                            <?= $field['label'] ?> <? if ($field['required']): ?><span
                                class="required">*</span><? endif; ?>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="number" id="f_<?= $key ?>"
                                   name="<?= $field['name'] ?>"
                                   class="form-control col-md-7 col-xs-12"
                                   value="<?= $field['value'] ?>"
                                <?= (!empty($field['placeholder']) ? 'placeholder="' . $field['placeholder'] . '"' : '') ?>
                                <?= ($field['required'] ? 'required="required"' : '') ?>
                            >
                            <? if (!empty($field['help'])): ?>
                                <span class="help-block"><?= $field['help'] ?></span>
                            <? endif; ?>
                        </div>
                    </div>
                    <?
                    break;
                case 'select':
                    ?>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="f_<?= $key ?>">
                            <?= $field['label'] ?> <? if ($field['required']): ?><span
                                class="required">*</span><? endif; ?>
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <? if($field['multiple']): ?>
                                <select name="<?= $field['name'] ?>"
                                        class="select2_multiple form-control"
                                        id="f_<?= $key ?>"
                                        multiple="multiple">
                                    <? foreach ($field['options'] as $value => $label): ?>
                                        <option value="<?= $value ?>"
                                            <?= (in_array($value, $field['value']) ? 'selected' : '') ?>
                                        ><?= $label ?></option>
                                    <? endforeach; ?>
                                </select>
                            <? else: ?>
                                <select name="<?= $field['name'] ?>" class="select2_single form-control" id="f_<?= $key ?>">
                                    <? foreach ($field['options'] as $value => $label): ?>
                                        <option value="<?= $value ?>"
                                            <?= ($value == $field['value'] ? 'selected' : '') ?>
                                        ><?= $label ?></option>
                                    <? endforeach; ?>
                                </select>
                            <? endif; ?>

                            <? if (!empty($field['help'])): ?>
                                <span class="help-block"><?= $field['help'] ?></span>
                            <? endif; ?>

                        </div>
                    </div>
                    <?
                    break;
                case 'radio':
                    ?>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?= $field['label'] ?>
                            <? if ($field['required']): ?>
                                <span class="required">*</span>
                            <? endif; ?>
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <? foreach ($field['options'] as $value => $label): ?>
                                <label>
                                    <input type="radio"
                                           class="flat"
                                           name="<?= $field['name'] ?>"
                                           value="<?= $value ?>"
                                        <?= ($value == $field['value'] ? 'checked=""' : '') ?>
                                        <?= ($field['required'] ? 'required="required"' : '') ?> />
                                    <?= $label ?>
                                </label>
                                <br>
                            <? endforeach; ?>
                            <? if (!empty($field['help'])): ?>
                                <span class="help-block"><?= $field['help'] ?></span>
                            <? endif; ?>
                        </div>
                    </div>
                    <?
                    break;
                case 'checkbox':
                    ?>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?= $field['label'] ?>
                            <? if ($field['required']): ?>
                                <span class="required">*</span>
                            <? endif; ?>
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <? foreach ($field['options'] as $value => $label): ?>
                                <label>
                                    <input type="checkbox"
                                           class="flat"
                                           name="<?= $field['name'] ?>"
                                           value="<?= $value ?>"
                                        <?= (in_array($value, $field['value']) ? 'checked="checked"' : '') ?>
                                        <?= ($field['required'] ? 'required="required"' : '') ?>
                                            /> <?= $label ?>
                                </label>
                                <br>
                            <? endforeach; ?>
                            <? if (!empty($field['help'])): ?>
                                <span class="help-block"><?= $field['help'] ?></span>
                            <? endif; ?>
                        </div>
                    </div>
                    <?
                    break;
                case 'textarea':
                    ?>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="f_<?= $key ?>"><?= $field['label'] ?>
                            <? if ($field['required']): ?>
                                <span class="required">*</span>
                            <? endif; ?>
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <textarea class="<?= $field['editor'] ? 'ckeditor' : '' ?> resizable_textarea form-control"
                                      name="<?= $field['name'] ?>"
                                      rows="8"
                                      id="f_<?= $key ?>"><?= $field['value'] ?></textarea>
                            <? if (!empty($field['help'])): ?>
                                <span class="help-block"><?= $field['help'] ?></span>
                            <? endif; ?>
                        </div>
                    </div>
                    <?
                    break;

                default:
                    echo '';
            endswitch;
            ?>
        <? endforeach; ?>

        <div class="ln_solid"></div>
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <a href="<?= $this->result['cancel'] ?>" class="btn btn-primary">Отменить</a>
                <button type="submit"
                        class="btn btn-success"
                        name="submit"
                        value="save">Сохранить</button>
            </div>
        </div>

    </form>

<? endif; ?>