<?php defined('_IN_JOHNCMS') or die('Restricted access');
/**
 * @var $this Component
 */

// $this->params - Содержит параметры компонента (массив)
$result = [];

// Обычные сообщения
$result['messages'] = [];
if(!empty($this->params['messages'])) {
    $result['messages'] = $this->params['messages'];
}

// Сообщения об ощибках
$result['errors'] = [];
if(!empty($this->params['errors'])) {
    $result['errors'] = $this->params['errors'];
}

// Страница на которую произойдёт переход при клике по кнопке "Отмена"
if(!empty($this->params['cancel'])) {
    $result['cancel'] = htmlspecialchars($this->params['cancel']);
} else {
    $result['cancel'] = '/admin/';
}

// Страница на которую будет происходить отправка данных формы
if(!empty($this->params['action'])) {
    $result['action'] = htmlspecialchars($this->params['action']);
} else {
    $result['action'] = '/admin/';
}

// Массив полей формы
if(!empty($this->params['fields'])) {
    $result['fields'] = $this->params['fields'];
} else {
    $result['fields'] = [];
}


/*
 * Пример массива полей
$result['fields'] = [
    [
        'type'        => 'text',
        'name'        => 'name',
        'label'       => 'Название',
        'placeholder' => 'Введите название',
        'help'        => 'Текст пояснения',
        'value'       => 'Значение',
        'required'    => true,
    ],
    [
        'type'     => 'textarea',
        'name'     => 'name_textarea',
        'label'    => 'Текстовое поле',
        'help'     => 'Текст пояснения',
        'value'    => 'Значение',
        'required' => true,
        'editor'   => true,
    ],
    [
        'type'        => 'number',
        'name'        => 'name_number',
        'label'       => 'Число',
        'placeholder' => 'Введите число',
        'help'        => 'Текст пояснения',
        'value'       => '1234',
        'required'    => true,
    ],
    [
        'type'     => 'checkbox',
        'name'     => 'name_checkbox[]',
        'label'    => 'Чекбокс',
        'help'     => 'Текст пояснения',
        'required' => true,
        'value'    => [1, 4, 2],
        'options'  => [
            '1' => 'Вариант 1',
            '2' => 'Вариант 2',
            '3' => 'Вариант 3',
            '4' => 'Вариант 4',
        ],
    ],
    [
        'type'     => 'radio',
        'name'     => 'name_radio',
        'label'    => 'Радиокнопки',
        'help'     => 'Текст пояснения',
        'value'    => '2',
        'required' => true,
        'options'  => [
            '1' => 'Вариант 1',
            '2' => 'Вариант 2',
            '3' => 'Вариант 3',
            '4' => 'Вариант 4',
        ],
    ],
    [
        'type'        => 'select',
        'name'        => 'name_select_multi[]',
        'label'       => 'Мультиселект',
        'placeholder' => 'Выберите из списка',
        'help'        => 'Текст пояснения',
        'value'       => [1, 4, 2],
        'multiple'    => true,
        'required'    => true,
        'options'     => [
            '1' => 'Вариант 1',
            '2' => 'Вариант 2',
            '3' => 'Вариант 3',
            '4' => 'Вариант 4',
        ],
    ],
    [
        'type'        => 'select',
        'name'        => 'name_select',
        'label'       => 'Селект',
        'placeholder' => 'Выберите из списка',
        'help'        => 'Текст пояснения',
        'value'       => '1',
        'multiple'    => false,
        'required'    => true,
        'options'     => [
            '1' => 'Вариант 1',
            '2' => 'Вариант 2',
            '3' => 'Вариант 3',
            '4' => 'Вариант 4',
        ],
    ]
];
*/


// Массив который попадает в шаблон
$this->result = $result;

// Загружаем языки
$this->loadLanguage();

// Загружаем шаблон
$this->loadTemplate();
