<?php defined('_IN_JOHNCMS') or die('Restricted access');
/**
 * @var $this Component
 */

// $this->params - Содержит параметры компонента (массив)
$result = [];

$this->params['type'] = isset($this->params['type']) ? intval($this->params['type']) : 0;

// Сообщения об ощибках
$result['errors'] = [];

PageBuffer::getInstance()->setTitle('Добавить инфоблок');

$result['fields'] = [
    [
        'type'        => 'text',
        'name'        => 'name',
        'label'       => 'Название инфоблока',
        'placeholder' => 'Введите название',
        'help'        => '',
        'value'       => (isset($_POST['name']) ? htmlspecialchars($_POST['name']) : ''),
        'required'    => true,
    ],
    [
        'type'        => 'text',
        'name'        => 'code',
        'label'       => 'Код инфоблока',
        'placeholder' => 'Введите код инфоблока',
        'help'        => 'Разрешены только латинские буквы, цифры и знак нижнего подчеркивания',
        'value'       => (isset($_POST['code']) ? htmlspecialchars($_POST['code']) : ''),
        'required'    => true,
    ],
    [
        'type'        => 'text',
        'name'        => 'table_name',
        'label'       => 'Название таблицы инфоблока',
        'placeholder' => 'Таблица инфоблока',
        'help'        => 'Разрешены только латинские буквы, цифры и знак нижнего подчеркивания',
        'value'       => (isset($_POST['table_name']) ? htmlspecialchars($_POST['table_name']) : ''),
        'required'    => true,
    ]
];

if (isset($_POST['submit']) && $_POST['submit'] == 'save') {
    $blockRes = new System\Blocks\IBlocks();

    // Добавляем тип инфоблока
    $added = $blockRes->add([
        'name'       => $_POST['name'],
        'code'       => $_POST['code'],
        'table_name' => $_POST['table_name'],
        'type'       => $this->params['type'],
    ]);

    // Если добавлен, возвращаемся в список
    if ($added) {
        header('Location: /admin/settings/iblocks/list/?type=' . $this->params['type']);
        exit;
    } else {
        // Если добавить не удалось, возвращаем ошибки
        $result['errors'] = $blockRes->last_errors;
    }
}


// Массив который попадает в шаблон
$this->result = $result;

// Загружаем языки
$this->loadLanguage();

// Загружаем шаблон
$this->loadTemplate();
