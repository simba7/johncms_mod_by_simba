<?php defined('_IN_JOHNCMS') or die('Error: restricted access');
/**
 * @var $this Component
 */
?>

<? core::loadComponent('johnadmin', 'form', 'default', [
    'fields' => $this->result['fields'],
    'errors' => $this->result['errors'],
    'action' => '/admin/settings/iblocks/edit/?id=' . $this->params['iblock'],
    'cancel' => '/admin/settings/iblocks/list/?type=' . $this->params['type']
]); ?>