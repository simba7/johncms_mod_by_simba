<?php defined('_IN_JOHNCMS') or die('Restricted access');
/**
 * @var $this Component
 */

// $this->params - Содержит параметры компонента (массив)
$result = [];

if (empty($this->params['iblock']) && intval($this->params['iblock']) < 1) {
    exit('Не задан обязательный параметр компонента "iblock" - ID инфоблока');
}

// Сообщения об ощибках
$result['errors'] = [];

PageBuffer::getInstance()->setTitle('Изменить инфоблок');

$blockRes = new System\Blocks\IBlocks();
// Получаем данные инфоблока
$arr_iblock = $blockRes->getByID($this->params['iblock']);

$this->params['type'] = $arr_iblock['type'];

// Формируем массив полей
$result['fields'] = [
    [
        'type'        => 'text',
        'name'        => 'name',
        'label'       => 'Название инфоблока',
        'placeholder' => 'Введите название',
        'help'        => '',
        'value'       => (isset($_POST['name']) ? htmlspecialchars($_POST['name']) : $arr_iblock['name']),
        'required'    => true,
    ],
    [
        'type'        => 'text',
        'name'        => 'code',
        'label'       => 'Код инфоблока',
        'placeholder' => 'Введите код инфоблока',
        'help'        => 'Разрешены только латинские буквы, цифры и занк нижнего подчеркивания',
        'value'       => (isset($_POST['code']) ? htmlspecialchars($_POST['code']) : $arr_iblock['code']),
        'required'    => true,
    ]
];

if (isset($_POST['submit']) && $_POST['submit'] == 'save') {

    // Обновляем данные инфоблока
    $added = $blockRes->update($arr_iblock['id'], ['name' => $_POST['name'], 'code' => $_POST['code']]);

    // Если обновлен, возвращаемся в список
    if ($added) {
        header('Location: /admin/settings/iblocks/list/?type=' . $this->params['type']);
        exit;
    } else {
        // Если обновить не удалось, возвращаем ошибки
        $result['errors'] = $blockRes->last_errors;
    }
}

// Массив который попадает в шаблон
$this->result = $result;

// Загружаем языки
$this->loadLanguage();

// Загружаем шаблон
$this->loadTemplate();
