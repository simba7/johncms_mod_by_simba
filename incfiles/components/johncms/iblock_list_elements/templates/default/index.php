<?php defined('_IN_JOHNCMS') or die('Error: restricted access');
/**
 * @var $this Component
 * @var $result
 */
?>

<? if (!empty($this->result) && !empty($this->result['items'])): ?>
    <div class="news-list">
        <? foreach ($result['items'] as $item): ?>
        <div class="news-item">
            <h3><?= $item['f_name'] ?></h3>

            <div class="preview-text">
                <?= $item['f_anonce'] ?>
            </div>
            <div class="author">
                (<?= $item['date_created'] ?>)
            </div>
        </div>
        <? endforeach; ?>
    </div>
    <?= $result['pagination'] ?>
    <div class="total-items">Всего новостей: <?= $result['count_elements'] ?></div>
<? endif; ?>
