<?php defined('_IN_JOHNCMS') or die('Restricted access');
/**
 * @var $this Component
 */

// $this->params - Содержит параметры компонента (массив)
$result = array();


if (!empty($this->params['menu_type'])) {

    // Проверка корректности имени файла меню (только фраза с названием)
    if (!preg_match('#^[a-zA-Z0-9_]+$#', $this->params['menu_type'])) {
        $this->errors[] = 'Incorrect menu type';

        return;
    }

    // Проверяем наличие файла меню нужного нам типа
    if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/' . $this->params['menu_type'] . '.menu.php')) {
        $result = require($_SERVER['DOCUMENT_ROOT'] . '/' . $this->params['menu_type'] . '.menu.php');

        // Ставим пункту меню метку активности
        $current_url = $_SERVER['REQUEST_URI'];
        foreach ($result as $key => $item) {
            if (!empty($item['url']) && $item['url'] != '/') {
                $path = preg_quote($item['url'], '/');
                if (preg_match('/^' . $path . '/i', $current_url, $matches)) {
                    $result[$key]['active'] = 'yes';
                }
            }
            if ($item['url'] == '/' && $current_url == '/') {
                $result[$key]['active'] = 'yes';
            }
        }
    }
}

// Массив который попадает в шаблон
$this->result = $result;

// Загружаем языки
$this->loadLanguage();

// Загружаем шаблон
$this->loadTemplate();
