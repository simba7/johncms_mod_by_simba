<?php defined('_IN_JOHNCMS') or die('Restricted access');
/**
 * @package     JohnCMS
 * @link        http://johncms.com
 * @copyright   Copyright (C) 2008-2011 JohnCMS Community
 * @license     LICENSE.txt (see attached file)
 * @version     VERSION.txt (see attached file)
 * @author      http://johncms.com/about
 */

/**
 * Class Component
 *
 * @package JohnCMS
 * @author  Maxim (Simba) Masalov <max@symbos.su>
 */
class Component
{

    /**
     * Название компонента
     * @var string
     */
    public $name = '';

    /**
     * Язык
     * @var string
     */
    public $language = '';

    /**
     * Шаблон компонента
     * @var string
     */
    public $template = '';

    /**
     * Шаблон сайта
     * @var string
     */
    public $site_template = '';

    /**
     * Разработчик компонента
     * @var string
     */
    public $developer = '';

    /**
     * Параметры передаваемые в компонент
     * @var array
     */
    public $params = array();

    /**
     * Ошибки
     * @var array
     */
    public $errors = array();
    
    /**
     * Массив языковых фраз
     * @var array
     */
    public $lang = array();

    /**
     * Результирующий массив который попадает в шаблон
     * @var array
     */
    public $result = array();

    private $path = '';

    public function __construct()
    {
        $this->language = core::$lng_iso;
        $this->site_template = core::$system_set['skindef'];

    }


    /**
     * Подключаем компонент
     *
     * @param $developer
     * @param $name
     * @param string $template
     * @param array $params
     * @return bool|mixed|string
     */
    public function loadComponent($developer, $name, $template = 'default', $params = array())
    {
        if (!empty($developer) && !empty($name)) {
            $this->developer = $developer;
            $this->name = $name;
            if (empty($template)) {
                $this->template = 'default';
            } else {
                $this->template = $template;
            }

            $this->params = $params;
        } else {
            $this->errors[] = 'Empty component name or developer';
        }

        if (empty($this->errors) && $this->checkComponent()) {
            return include($this->path);
        } else {
            if (!empty($this->errors)) {
                return functions::display_error($this->errors);
            } else {
                return functions::display_error('Component "' . $this->name . '" not loaded');
            }
        }

    }


    /**
     * Метод проверяет корректность указанных разработчика и компонента и наличие такового
     *
     * @return bool
     */
    private function checkComponent()
    {
        if (!preg_match('#^[a-zA-Z0-9_]+$#', $this->developer)) {
            $this->errors[] = 'Incorrect developer name';

            return false;
        }

        if (!preg_match('#^[a-zA-Z0-9_]+$#', $this->name)) {
            $this->errors[] = 'Incorrect component name';

            return false;
        }

        $path = $_SERVER['DOCUMENT_ROOT'] . '/incfiles/components/' . $this->developer . '/' . $this->name . '/index.php';

        if (file_exists($path)) {
            $this->path = $path;

            return true;
        } else {
            $this->errors[] = 'Component not found';

            return false;
        }
    }


    /**
     * Метод загружает языковой файл из папки с шаблоном
     *
     * @return bool
     */
    public function loadLanguage()
    {
        if (!preg_match('#^[a-zA-Z0-9_]+$#', $this->language)) {
            $this->errors[] = 'Incorrect language name';
        }

        $on_template = $_SERVER['DOCUMENT_ROOT'] . '/theme/' . $this->site_template . '/components/' . $this->developer . '/' . $this->name . '/' . $this->template . '/lang/' . $this->language . '.lng';
        $on_component = $_SERVER['DOCUMENT_ROOT'] . '/incfiles/components/' . $this->developer . '/' . $this->name . '/templates/' . $this->template . '/lang/' . $this->language . '.lng';
        $default = $_SERVER['DOCUMENT_ROOT'] . '/incfiles/components/' . $this->developer . '/' . $this->name . '/templates/default/lang/' . $this->language . '.lng';
        if (file_exists($on_template)) {
            $lang_file = $on_template;
        } elseif (file_exists($on_component)) {
            $lang_file = $on_component;
        } elseif (file_exists($default)) {
            $lang_file = $default;
        } else {
            $lang_file = '';
        }

        if (!empty($lang_file)) {
            $this->lang = parse_ini_file($lang_file, true);
        }
    }


    /**
     * Метод загружает шаблон компонента
     *
     * @return bool|mixed
     */
    public function loadTemplate()
    {
        if (!preg_match('#^[a-zA-Z0-9_]+$#', $this->template)) {
            $this->errors[] = 'Incorrect template name';

            return false;
        }

        // Проверяем наличие шаблона в разных папках по приоритету
        $on_template = $_SERVER['DOCUMENT_ROOT'] . '/theme/' . $this->site_template . '/components/' . $this->developer . '/' . $this->name . '/' . $this->template . '/index.php';
        $on_component = $_SERVER['DOCUMENT_ROOT'] . '/incfiles/components/' . $this->developer . '/' . $this->name . '/templates/' . $this->template . '/index.php';
        $default = $_SERVER['DOCUMENT_ROOT'] . '/incfiles/components/' . $this->developer . '/' . $this->name . '/templates/default/index.php';


        if (file_exists($on_template)) {
            $template_file = $on_template;
        } elseif (file_exists($on_component)) {
            $template_file = $on_component;
        } elseif (file_exists($default)) {
            $template_file = $default;
        } else {
            $this->errors[] = 'Template not found';

            return false;
        }

        // Переменные будут доступны в шаблоне
        $result = $this->result;
        $params = $this->params;
        $lang = $this->lang;

        return include($template_file);
    }

}