<?php defined('_IN_JOHNCMS') or die('Restricted access');
/**
 * @package     JohnCMS
 * @link        http://johncms.com
 * @copyright   Copyright (C) 2008-2011 JohnCMS Community
 * @license     LICENSE.txt (see attached file)
 * @version     VERSION.txt (see attached file)
 * @author      http://johncms.com/about
 */

/**
 * Class pageBuffer
 *
 * @package JohnCMS
 * @author  Maxim (Simba) Masalov <max@symbos.su>
 */
class PageBuffer
{
    protected static $_instance;

    /**
     * Массив со всеми частями буфера вывода
     * (разбивка на части происходит при вызове отложенных функций)
     * @var array
     */
    private $buffer = array();

    /**
     * Массив отложенных функций
     * @var array
     */
    private $buffer_callback = array();

    /**
     * HTTP заголовки
     * @var array
     */
    private $headers = array();

    /**
     * Подключаемые CSS файлы
     * @var array
     */
    private $styles = array();

    /**
     * Подключаемые JS файлы
     * @var array
     */
    private $scripts = array();

    /**
     * META теги
     * @var array
     */
    private $meta_tags = array();

    /**
     * Хлебные крошки
     * @var array
     */
    private $breadcrumbs = array();

    /**
     * Заголовок страницы <title></title>
     * @var string
     */
    private $page_title = '';

    /**
     * H1 заголовок
     * @var string
     */
    private $page_header = '';


    private function __construct()
    {
    }


    private function __clone()
    {
    }


    /**
     * @return PageBuffer
     */
    public static function getInstance()
    {

        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }


    /**
     * Начало буферизации вывода
     */
    public function startBuffer()
    {
        $this->addHeader('X-Powered-CMS', 'JohnCMS mod by Simba (2.0.0)');
        $this->addMeta('Generator', 'JohnCMS, http://johncms.com');
        ob_start();
    }


    /**
     * Метод добавляет функцию для отложенного выполнения
     *
     * @param $callback
     */
    public function addBuffer($callback)
    {
        $args = array();
        $args_num = func_num_args();
        if ($args_num > 1) {
            for ($i = 1; $i < $args_num; $i++) {
                $args[] = func_get_arg($i);
            }
        }

        $this->buffer[] = ob_get_contents();
        $this->buffer[] = '';
        $this->buffer_callback[] = array('function' => $callback, 'params' => $args);
        ob_end_clean();

        ob_start();
    }


    /**
     * Завершение буферизации вывода, выполнение отложенныъ функций и выдача контента
     *
     * @return string
     */
    public function endBuffer()
    {
        $content = ob_get_contents();
        ob_end_clean();
        $cnt = count($this->buffer_callback);
        for ($i = 0; $i < $cnt; $i++) {
            $this->buffer[$i * 2 + 1] = @call_user_func_array($this->buffer_callback[$i]["function"],
                $this->buffer_callback[$i]["params"]);
        }
        $content = implode('', $this->buffer) . $content;

        // Устанавливаем заголовки ответа
        if (is_array($this->headers)) {
            foreach ($this->headers as $header) {
                $this->setHeader($header);
            }
        }

        return $content;
    }


    /**
     * Метод добавляет http заголовок
     *
     * @param $name
     * @param string $value
     */
    public function addHeader($name, $value = '')
    {
        if (empty($name)) {
            return;
        }

        if (empty($value)) {
            $this->headers[] = $name;
        } else {
            $this->headers[] = array($name, $value);
        }
    }


    /**
     * Метод добавляет элемент в навигационную цепочку
     *
     * @param $name
     * @param string $url
     */
    public function addChain($name, $url = '')
    {
        if (empty($name)) {
            return;
        }

        $name = htmlspecialchars($name);
        if (!empty($url)) {
            $url = htmlspecialchars($url);
        }

        $this->breadcrumbs[] = array($name, $url);
    }


    /**
     * Возвращает массив элементов навигационной цепочки
     *
     * @return array
     */
    public function getNavArray()
    {
        return $this->breadcrumbs;
    }


    /**
     * Метод устанавливает http заголовки.
     *
     * @param $header
     */
    protected function setHeader($header)
    {
        if (is_array($header)) {
            header(sprintf("%s: %s", $header[0], $header[1]));
        } else {
            header($header);
        }
    }


    /**
     * Метод добавляет скрипт в массив скриптов
     * @param $path
     */
    public function addJs($path)
    {
        if (!empty($path)) {
            $this->scripts[] = htmlspecialchars($path);
        }
    }


    /**
     * Метод добавляет CSS стиль в массив стилей
     * @param $path
     */
    public function addCss($path)
    {
        if (!empty($path)) {
            $this->styles[] = htmlspecialchars($path);
        }
    }


    /**
     * Метод возвращает заголовок страницы (title или h1 если передать true)
     * @param bool|false $title - В случае передачи true, вернет заголовок H1
     * @return string
     */
    public function getTitle($title = false)
    {
        if ($title !== false && !empty($this->page_header)) {
            return $this->page_header;
        }

        if (!empty($this->page_title)) {
            return $this->page_title;
        } else {
            return core::$system_set['copyright'];
        }
    }


    /**
     * Метод устанавливает заголовок страницы
     * @param string $title - Текст заголовка
     * @param bool|false $type - Если передать true будет задан заголовок H1
     */
    public function setTitle($title = '', $type = false)
    {
        if (!empty($title) && $type === false) {
            $this->page_title = htmlspecialchars($title);
        } elseif (!empty($title) && $type === true) {
            $this->page_header = htmlspecialchars($title);
        }
    }


    /**
     * Возвращает списко JS
     * @return string
     */
    private function getJs()
    {
        $scripts = '';
        // Устанавливаем заголовки ответа
        if (is_array($this->scripts)) {
            foreach ($this->scripts as $script) {
                $scripts .= '<script src="' . $script . '"></script>' . "\n";
            }
        }

        return $scripts;
    }


    /**
     * Возвращает списко CSS
     * @return string
     */
    private function getCss()
    {
        $styles = '';
        // Устанавливаем заголовки ответа
        if (is_array($this->styles)) {
            foreach ($this->styles as $style) {
                $styles .= '<link href="' . $style . '" rel="stylesheet">' . "\n";
            }
        }

        return $styles;
    }


    /**
     * Получает мета теги и выводит их в строку
     * @return string
     */
    private function getMeta()
    {
        $meta = '';
        // Устанавливаем заголовки ответа
        if (is_array($this->meta_tags)) {
            foreach ($this->meta_tags as $name => $val) {
                $meta .= '<meta name="' . htmlspecialchars($name) . '" content="' . htmlspecialchars($val) . '">' . "\n";
            }
        }

        return $meta;
    }


    /**
     * Добавляет мета тег
     * @param $name - Название тега
     * @param string $value - Значение
     */
    public function addMeta($name, $value = '')
    {
        if (!empty($name)) {
            $this->meta_tags[$name] = $value;
        }
    }


    /**
     * Метод для отображения заголовка
     * @param bool|false $header
     */
    public function showTitle($header = false)
    {
        $this->addBuffer(array(&$this, 'getTitle'), $header);
    }


    /**
     * Метод для отображения meta
     */
    public function showMeta()
    {
        $this->addBuffer(array(&$this, 'getMeta'));
    }


    /**
     * Метод для отображения JS
     */
    public function showJs()
    {
        $this->addBuffer(array(&$this, 'getJs'));
    }


    /**
     * Метод для отображения CSS
     */
    public function showCss()
    {
        $this->addBuffer(array(&$this, 'getCss'));
    }


    /**
     * Отображает хлебные крошки
     */
    public function showBreadCrumbs()
    {
        $this->addBuffer(array('functions', 'showNavChain'));
    }


    /**
     * Очищаем буфер
     */
    public function cleanBuffer()
    {
        ob_end_clean();
    }

}