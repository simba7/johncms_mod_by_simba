<?php defined('_IN_JOHNCMS') or die('No direct script access.');

/**
 * @author Maxim Masalov
 * @file config.php
 * @created 04.03.2015 17:16
 * @deprecated Используйте \System\Core\Config
 */
class Config
{

    /**
     * @var object - Singleton
     */
    protected static $_instance;

    /**
     * Directory for config files
     * @var string
     */
    protected $config_path = '';

    /**
     * Already read configs
     * @var array
     */
    protected $configs = array();


    /**
     * Class constructor
     */
    private function __construct()
    {
        $this->config_path = $_SERVER['DOCUMENT_ROOT'] . '/incfiles/configs/';
    }


    /**
     * Close access to functions outside of the class
     */
    private function __clone()
    {
    }


    /**
     * This is singleton pattern
     * @return Config
     */
    public static function getInstance()
    {
        // Check actuality instance
        if (null === self::$_instance) {
            // Create new instance
            self::$_instance = new self();
        }
        // return instance
        return self::$_instance;
    }


    /**
     * Loading config
     * @param $name
     * @return array|mixed
     */
    function load($name)
    {
        // If the configuration file already read then return it
        if (array_key_exists($name, $this->configs)) {
            return $this->configs[$name];
        }

        // Reading config if exists
        if (file_exists($this->config_path . $name . '.php')) {
            $this->configs[$name] = require_once($this->config_path . $name . '.php');
        }

        return $this->configs[$name];
    }

}