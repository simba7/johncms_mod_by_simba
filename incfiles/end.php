<?php

/**
 * @package     JohnCMS
 * @link        http://johncms.com
 * @copyright   Copyright (C) 2008-2011 JohnCMS Community
 * @license     LICENSE.txt (see attached file)
 * @version     VERSION.txt (see attached file)
 * @author      http://johncms.com/about
 */
defined('_IN_JOHNCMS') or die('Error: restricted access');

if(file_exists(ROOTPATH.'theme/'.$set_user['skin'].'/footer.php')) {
    require_once ROOTPATH.'theme/'.$set_user['skin'].'/footer.php';
}
echo PageBuffer::getInstance()->endBuffer();
