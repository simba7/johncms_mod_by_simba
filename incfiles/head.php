<?php

/**
 * @package     JohnCMS
 * @link        http://johncms.com
 * @copyright   Copyright (C) 2008-2011 JohnCMS Community
 * @license     LICENSE.txt (see attached file)
 * @version     VERSION.txt (see attached file)
 * @author      http://johncms.com/about
 *
 * @var $set_user
 */
if (!defined('_IN_JOHNCMS')) {
    define('_IN_JOHNCMS', 1);
}

define('START_TIME', microtime(TRUE));

if (!defined('ROOTPATH')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/incfiles/core.php';
}


if (file_exists(ROOTPATH . 'theme/' . $set_user['skin'] . '/header.php')) {
    require_once ROOTPATH . 'theme/' . $set_user['skin'] . '/header.php';
}

