<?php
/**
 * Created by PhpStorm.
 * User: Maxim Masalov
 * Date: 29.08.2016
 * Time: 18:23
 * Project: johncms_mod_by_simba
 */

namespace System\Blocks;


class IBProps
{
    public $last_errors = [];


    public function __construct()
    {
    }

    /**
     * @param int $iblock_id
     * @param array $params = [
     * type => string, text, int,
     * name => Название инфоблока,
     * code => Код свойства (латинские маленькие буквы),
     * multiple => y|n - Множественное или нет.
     * ]
     * @return bool
     */
    public function add($iblock_id, $params = [])
    {
        global $DB;

        if ($iblock_id < 1 || empty($params)) {
            return false;
        }

        if (empty($params['code']) || !preg_match('#^[a-zA-Z_]+$#', $params['code'])) {
            $this->last_errors[] = 'Код свойства должен состоять только из букв латинского алфавита и знака нижнего подчёркивания';
        }

        if (empty($params['type']) || !in_array($params['type'], ['string', 'text', 'int'])) {
            $this->last_errors[] = 'Указанный тип свойства не поддерживается';
        }

        $ib = new IBlocks();
        $ib_data = $ib->getByID($iblock_id);

        if (!empty($ib_data) && empty($this->last_errors)) {

            // Обновяем таблицу и добавляем поле
            if ($params['type'] == 'string') {
                self::addStringProperty($ib_data['table_name'], ['code' => $params['code']]);
            } elseif ($params['type'] == 'text') {
                self::addTextProperty($ib_data['table_name'], ['code' => $params['code']]);
            } elseif ($params['type'] == 'int') {
                self::addIntProperty($ib_data['table_name'], ['code' => $params['code']]);
            }

            // Сохраняем запись о свойстве в системную таблицу
            $DB->insert('s_blocks_props', [
                'name' => $params['name'],
                'code' => $params['code'],
                'type' => $params['type'],
                'multiple' => ($params['multiple'] == 'y' ? 1 : 0),
                'iblock_id' => $ib_data['id'],
            ]);

        } else {
            return false;
        }

        return true;
    }


    /**
     * Метод добавляет свойство типа "Строка"
     * @param string $table - Таблица для добавления
     * @param array $params - [
     * code - Название поля
     * ]
     * @return bool
     */
    private function addStringProperty($table, $params = [])
    {
        global $DB;

        if (empty($params)) {
            return false;
        }

        $DB->query('ALTER TABLE `s_b_' . $DB->toSql($table) . '` ADD `f_' . $DB->toSql($params['code']) . '` VARCHAR(255) NOT NULL;');

        return true;
    }


    /**
     * Метод добавляет свойство типа "Число"
     * @param string $table - Таблица для добавления
     * @param array $params - [
     * code - Название поля
     * ]
     * @return bool
     */
    private function addIntProperty($table, $params = [])
    {
        global $DB;

        if (empty($params)) {
            return false;
        }

        $DB->query('ALTER TABLE `s_b_' . $DB->toSql($table) . '` ADD `f_' . $DB->toSql($params['code']) . '` BIGINT NOT NULL;');

        return true;
    }


    /**
     * Метод добавляет свойство типа "Текст"
     * @param string $table - Таблица для добавления
     * @param array $params - [
     * code - Название поля
     * ]
     * @return bool
     */
    private function addTextProperty($table, $params = [])
    {
        global $DB;

        if (empty($params)) {
            return false;
        }

        $DB->query('ALTER TABLE `s_b_' . $DB->toSql($table) . '` ADD `f_' . $DB->toSql($params['code']) . '` LONGTEXT NOT NULL;');

        return true;
    }


    /**
     * Метод возващает параметры свойства по его id
     * @param int $id
     * @return array
     */
    public function getByID($id)
    {
        global $DB;

        $res = $DB->query('SELECT *, 
            a.`id` AS id, 
            a.`name` AS name, 
            a.`code` AS code, 
            a.`type` AS type, 
            b.`name` AS iblock_name, 
            b.`code` AS iblock_code, 
            b.`type` AS iblock_type 
            FROM `s_blocks_props` AS a 
            LEFT JOIN `s_blocks` AS b 
            ON a.`iblock_id` = b.`id`
            WHERE a.`id` = ' . intval($id) . ';');
        $arr = $DB->getAssoc($res);
        return $arr;
    }


    /**
     * Метод удаляет свойство инфоблока
     * @param int $id
     * @return bool
     */
    public function delete($id)
    {
        global $DB;
        $prop_arr = $this->getByID($id);

        if (!empty($prop_arr)) {

            // Удаляем поле из таблицы инфоблока
            $DB->query('ALTER TABLE `s_b_' . $prop_arr['table_name'] . '` DROP `f_' . $prop_arr['code'] . '`;');

            // Удаляем запись из таблицы со свойствами
            $DB->query('DELETE FROM `s_blocks_props` WHERE `id` = ' . intval($id));
            return true;
        }

        return false;
    }


    /**
     * Метод возвращает список свойств инфоблоков
     *
     * @param array $filter - Параметры фильтрации
     * ['field', 'condition', 'value'], ['field2', 'condition2', 'value2']
     * @return array
     */
    public function getList($filter = [])
    {
        global $DB;
        $arr = [];
        $where = '';
        if (!empty($filter) && is_array($filter)) {
            foreach ($filter as $item) {
                if (!empty($where)) {
                    $where .= ' AND ';
                }
                $where .= '`' . $DB->toSql($item[0]) . '` ' . $DB->toSql($item[1]) . ' ' . $DB->toSql($item[2]) . '';
            }
        }

        $sql = 'SELECT * FROM `s_blocks_props` ' . (!empty($where) ? ' WHERE ' . $where : '');
        $res = $DB->query($sql);

        while ($arr_item = $DB->getAssoc($res)) {
            $arr[] = $arr_item;
        }

        return $arr;
    }


}