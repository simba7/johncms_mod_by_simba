<?php
/**
 * Created by PhpStorm.
 * User: Maxim Masalov
 * Date: 20.10.2016
 * Time: 17:14
 * Project: johncms_mod_by_simba
 */

namespace System\Blocks;


class Sections
{

    public $iblock_data = [];

    public $last_errors = [];

    public $count_result = 0;


    public function __construct()
    {
    }


    /**
     * Метод добавляет раздел в инфоблок
     *
     * @param array $fields
     * @return int - ID добавленной записи
     */
    public function add($fields = [])
    {
        global $DB;

        $fields['iblock'] = isset($fields['iblock']) ? intval($fields['iblock']) : 0;
        $fields['parent_section'] = isset($fields['parent_section']) ? intval($fields['parent_section']) : 0;

        if ($fields['iblock'] < 1) {
            $this->last_errors[] = 'Не задан ID инфоблока';
        }

        if (empty($fields['name'])) {
            $this->last_errors[] = 'Не задано название раздела';
        }

        if (empty($this->last_errors)) {

            // Пытаемся получить правую границу родительской директории
            $res = $DB->query("SELECT `right`, `level` FROM `s_blocks_sections` WHERE `id` = " . $fields['parent_section'] . " AND `iblock` = " . $fields['iblock'] . ";");
            if ($arr = $DB->getAssoc($res)) {
                $treeLeft = $arr['left']+1;
                $treeRight = $arr['right']+2;
                $level = $arr['level'];
            } else {
                $treeLeft = 1;
                $treeRight = 2;
                $level = 1;
            }

            $DB->query("INSERT INTO s_blocks_sections SET 
                  `section` = " . $fields['parent_section'] . ", 
                  `iblock` = " . $fields['iblock'] . ", 
                  `name` = '" . $DB->toSql($fields['name']) . "',
                  `left` = '".$treeLeft."',
                  `right` = '".$treeRight."'
                  LEVEL = " . $level . ";
            ");
            $id = $DB->lastID();


            $DB->query("UPDATE s_blocks_sections SET `right` = ".$trRight." WHERE `id` = " . $fields['parent_section'] . " AND iblock = " . $fields['iblock'] . ";");


            // Пересчитываем границы дерева
            $DB->query("UPDATE s_blocks_sections SET `right` = `right` + 2 WHERE `right` > " . $treeRight . " AND iblock = " . $fields['iblock'] . ";");
            $DB->query("UPDATE s_blocks_sections SET `left` = `left` + 2 WHERE `left` > " . $treeRight . " AND iblock = " . $fields['iblock'] . ";");


            // Создаем раздел
            $DB->query("INSERT INTO s_blocks_sections SET `section` = " . $fields['parent_section'] . ", `iblock` = " . $fields['iblock'] . ", `name` = '" . $DB->toSql($fields['name']) . "', `left` = " . $treeRight . " + 1, `right` = " . $treeRight . " + 2, LEVEL = " . $level . " + 1;");


        } else {
            return false;
        }

        return true;
    }



}