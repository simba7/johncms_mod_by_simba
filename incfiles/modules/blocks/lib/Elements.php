<?php
/**
 * Created by PhpStorm.
 * User: Maxim Masalov
 * Date: 29.08.2016
 * Time: 22:27
 * Project: johncms_mod_by_simba
 */

namespace System\Blocks;


class Elements
{
    private $table = '';

    public $iblock_data = [];

    public $count_result = 0;


    /**
     * Elements constructor.
     * @param $id - Iblock id
     */
    public function __construct($id)
    {
        $ib = new IBlocks();
        $arr_iblock = $ib->getByID($id);
        if (isset($arr_iblock['table_name'])) {
            $this->table = $arr_iblock['table_name'];
        } else {
            $this->table = false;
        }

        $this->iblock_data = $arr_iblock;
    }


    /**
     * Метод получает список столбцов таблицы
     *
     * @return array
     */
    public function getColumns()
    {
        // Описываем стандартные поля
        $columns = [
            'id'           => 'ID',
            'date_created' => 'Создан',
            'date_updated' => 'Изменен'
        ];

        // Получаем свойства инфоблока
        $props = new IBProps();
        $arr_props = $props->getList([['iblock_id', '=', $this->iblock_data['id']]]);

        foreach ($arr_props as $prop) {
            $columns['f_' . $prop['code']] = $prop['name'];
        }

        return $columns;
    }


    /**
     * @param array $filter = [
     *  ['field', 'condition', 'value'], ['field2', 'condition2', 'value2']
     * ]
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public function getList($filter = [], $offset = 0, $limit = 10)
    {
        global $DB;
        $arr = [];
        $where = '';
        if (!empty($filter) && is_array($filter)) {
            foreach ($filter as $item) {
                if (!empty($where)) {
                    $where .= ' AND ';
                }
                $where .= '`' . $DB->toSql($item[0]) . '` ' . $DB->toSql($item[1]) . ' ' . $DB->toSql($item[2]) . '';
            }
        }


        $res_count = $DB->query('SELECT COUNT(*) FROM `s_b_' . $this->table . '` ' . (!empty($where) ? ' WHERE ' . $where : ''));
        $this->count_result = $DB->getCount($res_count);

        $sql = 'SELECT * FROM `s_b_' . $this->table . '` ' . (!empty($where) ? ' WHERE ' . $where : '') . ' LIMIT ' . intval($offset) . ', ' . intval($limit);

        $res = $DB->query($sql);

        while ($arr_item = $DB->getAssoc($res)) {
            $arr[] = $arr_item;
        }

        return $arr;
    }


    /**
     * Метод вставляет запись в инфоблок
     *
     * @param array $fields
     * @return int - ID добавленной записи
     */
    public function add($fields = [])
    {
        global $DB;

        $save_fields = [];

        // Получаем свойства, которые есть у инфоблока
        $props = new IBProps();
        $arr_props = $props->getList([['iblock_id', '=', $this->iblock_data['id']]]);

        // Проверяем поля. Выбираем только те поля для сохранения, которые есть в инфоблоке
        foreach ($arr_props as $prop) {
            if (array_key_exists('f_' . $prop['code'], $fields)) {
                $save_fields['f_' . $prop['code']] = $fields['f_' . $prop['code']];
            }
        }

        $DB->insert('s_b_' . $this->table, $save_fields);

        return $DB->lastID();
    }


    /**
     * Метод удаляет запись из инфоблока
     *
     * @param int $id - ID записи
     * @return bool
     */
    public function delete($id = 0)
    {
        global $DB;
        if (intval($id) > 0) {
            $DB->query('DELETE FROM `s_b_' . $this->table . '` WHERE `id` = ' . intval($id));
            return true;
        } else {
            return false;
        }
    }


    /**
     * Получает элемент инфоблока по ID
     * @param int $id - ID эемента
     * @return array
     */
    public function getByID($id = 0)
    {
        global $DB;
        if (intval($id) > 0) {
            $res = $DB->query('SELECT * FROM `s_b_' . $this->table . '` WHERE `id` = ' . intval($id));
            if ($arr_element = $DB->getAssoc($res)) {
                return $arr_element;
            } else {
                return [];
            }
        } else {
            return [];
        }
    }


    /**
     * Метод обновляет поля элемента инфоблока
     *
     * @param int $id - ID элемента
     * @param array $fields - Массив полей
     * @return bool
     */
    public function update($id = 0, $fields = [])
    {
        global $DB;

        if (array_key_exists('id', $fields)) {
            unset($fields['id']);
        }

        if (intval($id) > 0) {
            $DB->update('s_b_' . $this->table, $fields, 'WHERE `id` = ' . intval($id));
            return true;
        } else {
            return false;
        }
    }


}