<?php

$menu = array();

$menu[] = array(
    'url'  => '/news/',
    'name' => 'Новости'
);

$menu[] = array(
    'url'  => '/forum/',
    'name' => 'Форум'
);

$menu[] = array(
    'url'  => '/download/',
    'name' => 'Загрузки'
);

return $menu;
