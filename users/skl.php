<?php

/**
 * @package     JohnCMS
 * @link        http://johncms.com
 * @copyright   Copyright (C) 2008-2011 JohnCMS Community
 * @license     LICENSE.txt (see attached file)
 * @version     VERSION.txt (see attached file)
 * @author      http://johncms.com/about
 */

define('_IN_JOHNCMS', 1);

require('../incfiles/core.php');
$lng_pass = core::load_lng('pass');
$textl = $lng_pass['password_restore'];
require('../incfiles/head.php');
use System\Core\DB as DB;
function passgen($length) {
    $vals = "abcdefghijklmnopqrstuvwxyz0123456789";
    $result = '';
    for ($i = 1; $i <= $length; $i++) {
        $result .= $vals{rand(0, strlen($vals))};
    }
    return $result;
}

switch ($act) {
    case 'sent':
        /*
        -----------------------------------------------------------------
        Отправляем E-mail с инструкциями по восстановлению пароля
        -----------------------------------------------------------------
        */
        $nick = isset($_POST['nick']) ? functions::rus_lat(mb_strtolower(functions::check($_POST['nick']))) : '';
        $email = isset($_POST['email']) ? htmlspecialchars(trim($_POST['email'])) : '';
        $code = isset($_POST['code']) ? trim($_POST['code']) : '';
        $check_code = md5(rand(1000, 9999));
        $error = false;
        if (!$nick || !$email || !$code)
            $error = $lng['error_empty_fields'];
        elseif (!isset($_SESSION['code']) || mb_strlen($code) < 4 || $code != $_SESSION['code'])
            $error = $lng_pass['error_code'];
        unset($_SESSION['code']);
        if (!$error) {
            // Проверяем данные по базе
            $req = DB::getInstance()->query("SELECT * FROM `users` WHERE `name_lat` = '$nick' LIMIT 1");
            if (DB::getInstance()->numRows($req) == 1) {
                $res = DB::getInstance()->getAssoc($req);
                if (empty($res['mail']) || $res['mail'] != $email)
                    $error = $lng_pass['error_email'];
                if ($res['rest_time'] > time() - 86400)
                    $error = $lng_pass['restore_timelimit'];
            } else {
                $error = $lng['error_user_not_exist'];
            }
        }
        if (!$error) {
            // Высылаем инструкции на E-mail
            $subject = $lng_pass['password_restore'];
            $mail = $lng_pass['restore_help1'] . ', ' . $res['name'] . "\r\n" . $lng_pass['restore_help2'] . ' ' . $set['homeurl'] . "\r\n";
            $mail .= $lng_pass['restore_help3'] . ": \r\n" . $set['homeurl'] . "/users/skl.php?act=set&id=" . $res['id'] . "&code=" . $check_code . "\n\n";
            $mail .= $lng_pass['restore_help4'] . "\r\n";
            $mail .= $lng_pass['restore_help5'];
            $adds = "From: <" . $set['email'] . ">\r\n";
            $adds .= "Content-Type: text/plain; charset=\"utf-8\"\r\n";
            if (mail($res['mail'], $subject, $mail, $adds)) {
                DB::getInstance()->query("UPDATE `users` SET `rest_code` = '" . $check_code . "', `rest_time` = '" . time() . "' WHERE `id` = '" . $res['id'] . "'");
                ?>
                <div class="alert alert-dismissible alert-success">
                    <p><?=  $lng_pass['restore_help6'] ?></p>
                </div>
                <?
            } else {
                ?>
                <div class="alert alert-dismissible alert-danger">
                    <?= $lng_pass['error_email_sent'] ?>
                </div>
                <?
            }
        } else {
            // Выводим сообщение об ошибке
            echo functions::display_error($error, '<a href="skl.php">' . $lng['back'] . '</a>');
        }
        break;

    case 'set':
        /*
        -----------------------------------------------------------------
        Устанавливаем новый пароль
        -----------------------------------------------------------------
        */
        $code = isset($_GET['code']) ? trim($_GET['code']) : '';
        $error = false;
        if (!$id || !$code)
            $error = $lng['error_wrong_data'];
        $req = DB::getInstance()->query("SELECT * FROM `users` WHERE `id` = '$id'");
        if (DB::getInstance()->numRows($req)) {
            $res = DB::getInstance()->getAssoc($req);
            if (empty($res['rest_code']) || empty($res['rest_time'])) {
                $error = $lng_pass['error_fatal'];
            }
            if (!$error && ($res['rest_time'] < time() - 3600 || $code != $res['rest_code'])) {
                $error = $lng_pass['error_timelimit'];
                DB::getInstance()->query("UPDATE `users` SET `rest_code` = '', `rest_time` = '' WHERE `id` = '$id'");
            }
        } else {
            $error = $lng['error_user_not_exist'];
        }
        if (!$error) {
            // Высылаем пароль на E-mail
            $pass = passgen(4);
            $subject = $lng_pass['your_new_password'];
            $mail = $lng_pass['restore_help1'] . ', ' . $res['name'] . "\r\n" . $lng_pass['restore_help8'] . ' ' . $set['homeurl'] . "\r\n";
            $mail .= $lng_pass['your_new_password'] . ": $pass\r\n";
            $mail .= $lng_pass['restore_help7'];
            $adds = "From: <" . $set['email'] . ">\n";
            $adds .= "Content-Type: text/plain; charset=\"utf-8\"\r\n";
            if (mail($res['mail'], $subject, $mail, $adds)) {
                DB::getInstance()->query("UPDATE `users` SET `rest_code` = '', `password` = '" . md5(md5($pass)) . "' WHERE `id` = '$id'");
                ?>
                <div class="alert alert-dismissible alert-success">
                    <strong><?= $lng_pass['change_password'] ?></strong><br>
                    <p><?=  $lng_pass['change_password_conf'] ?></p>
                </div>
                <?
            } else {
                ?>
                <div class="alert alert-dismissible alert-danger">
                    <?= $lng_pass['error_email_sent'] ?>
                </div>
                <?
            }
        } else {
            // Выводим сообщение об ошибке
            echo functions::display_error($error);
        }
        break;

    default:
        /*
        -----------------------------------------------------------------
        Форма для восстановления пароля
        -----------------------------------------------------------------
        */
        ?>

        <form class="form-horizontal" action="skl.php?act=sent" method="post">
            <fieldset>
                <legend><?= $lng_pass['password_restore'] ?></legend>
                <div class="form-group">
                    <label for="inputLogin" class="col-lg-2 control-label"><?= $lng_pass['your_login'] ?></label>

                    <div class="col-lg-10">
                        <input type="text" name="nick" class="form-control" id="inputLogin" placeholder="<?= $lng_pass['your_login'] ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail" class="col-lg-2 control-label"><?= $lng_pass['your_email'] ?></label>

                    <div class="col-lg-10">
                        <input type="text" name="email" class="form-control" id="inputEmail" placeholder="<?= $lng_pass['your_email'] ?>">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-10">
                        <img src="/captcha.php?r=<?= rand(1000, 9999) ?>" alt="<?= $lng_pass['captcha'] ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="captcha_word" class="col-lg-2 control-label"><?= $lng_pass['enter_code'] ?></label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" id="captcha_word" name="code" placeholder="<?= $lng_pass['enter_code'] ?>" value="" autocomplete="off">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-2">
                        <button type="submit" class="btn btn-primary" value="<?= $lng_pass['sent'] ?>"><?= $lng_pass['sent'] ?></button>
                    </div>
                </div>
            </fieldset>
        </form>
        <div class="alert alert-dismissible alert-warning">
            <?= $lng_pass['restore_help'] ?>
        </div>

        <?
        break;
}

require('../incfiles/end.php');
?>