<?php

/*
////////////////////////////////////////////////////////////////////////////////
// JohnCMS                Mobile Content Management System                    //
// Project site:          http://johncms.com                                  //
// Support site:          http://gazenwagen.com                               //
////////////////////////////////////////////////////////////////////////////////
// Lead Developer:        Oleg Kasyanov   (AlkatraZ)  alkatraz@gazenwagen.com //
// Development Team:      Eugene Ryabinin (john77)    john77@gazenwagen.com   //
//                        Dmitry Liseenko (FlySelf)   flyself@johncms.com     //
////////////////////////////////////////////////////////////////////////////////
*/

defined('_IN_JOHNCMS') or die('Error: restricted access');
$headmod = 'userstop';
$textl = $lng['users_top'];
require('../incfiles/head.php');
use System\Core\DB as DB;
/*
-----------------------------------------------------------------
Функция отображения списков
-----------------------------------------------------------------
*/
function get_top($order = 'postforum') {
    global $lng;
    $req = DB::getInstance()->query("SELECT * FROM `users` WHERE `$order` > 0 ORDER BY `$order` DESC LIMIT 9");

    if (DB::getInstance()->numRows($req)) {
        $out = '';
        $i = 0;
        while ($res = DB::getInstance()->getAssoc($req)) {
            $out .= $i % 2 ? '<div class="list2">' : '<div class="list1">';
            $out .= functions::display_user($res, array ('header' => ('<b>' . $res[$order]) . '</b>')) . '</div>';
            ++$i;
        }
        return $out;
    } else {
        return '<div class="alert alert-dismissible alert-warning"><p>' . $lng['list_empty'] . '</p></div>';
    }
}

/*
-----------------------------------------------------------------
Меню выбора
-----------------------------------------------------------------
*/
    ?>
    <h1><?= $lng['community'] ?></h1>
    <ul class="nav nav-pills small-nav">
        <li <?= (!$mod ? 'class="active"' : '') ?>>
            <a href="index.php?act=top"><?= $lng['forum'] ?></a>
        </li>
        <li <?= ($mod == 'guest' ? 'class="active"' : '') ?>>
            <a href="index.php?act=top&amp;mod=guest"><?= $lng['guestbook'] ?></a>
        </li>
        <li <?= ($mod == 'comm' ? 'class="active"' : '') ?>>
            <a href="index.php?act=top&amp;mod=comm"><?= $lng['comments'] ?></a>
        </li>
        <? if ($set_karma['on']): ?>
            <li <?= ($mod == 'karma' ? 'class="active"' : '') ?>>
                <a href="index.php?act=top&amp;mod=karma"><?= $lng['karma'] ?></a>
            </li>
        <? endif; ?>
    </ul>

<?


switch ($mod) {
    case 'guest':
        /*
        -----------------------------------------------------------------
        Топ Гостевой
        -----------------------------------------------------------------
        */
        echo get_top('postguest');
        echo '<div class="phdr"><a href="../guestbook/index.php">' . $lng['guestbook'] . '</a></div>';
        break;

    case 'comm':
        /*
        -----------------------------------------------------------------
        Топ комментариев
        -----------------------------------------------------------------
        */
        echo get_top('komm');
        echo '<div class="phdr"><a href="../index.php">' . $lng['homepage'] . '</a></div>';
        break;

    case 'karma':
        /*
        -----------------------------------------------------------------
        Топ Кармы
        -----------------------------------------------------------------
        */
        if ($set_karma['on']) {
            $req = DB::getInstance()->query("SELECT *, (`karma_plus` - `karma_minus`) AS `karma` FROM `users` WHERE (`karma_plus` - `karma_minus`) > 0 ORDER BY `karma` DESC LIMIT 9");
            if (DB::getInstance()->numRows($req)) {
                while ($res = DB::getInstance()->getAssoc($req)) {
                    echo $i % 2 ? '<div class="list2">' : '<div class="list1">';
                    echo functions::display_user($res, array ('header' => ('<b>' . $res['karma']) . '</b>')) . '</div>';
                    ++$i;
                }
            } else {
                echo '<div class="alert alert-dismissible alert-warning"><p>' . $lng['list_empty'] . '</p></div>';
            }
            echo '<div class="phdr"><a href="../index.php">' . $lng['homepage'] . '</a></div>';
        }
        break;

    default:
        /*
        -----------------------------------------------------------------
        Топ Форума
        -----------------------------------------------------------------
        */
        echo get_top('postforum');
        echo '<div class="phdr"><a href="../forum/index.php">' . $lng['forum'] . '</a></div>';
}
echo '<p><a href="index.php">' . $lng['back'] . '</a></p>';
?>