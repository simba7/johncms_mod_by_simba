<?php

/**
 * @package     JohnCMS
 * @link        http://johncms.com
 * @copyright   Copyright (C) 2008-2011 JohnCMS Community
 * @license     LICENSE.txt (see attached file)
 * @version     VERSION.txt (see attached file)
 * @author      http://johncms.com/about
 */

define('_IN_JOHNCMS', 1);

$headmod = 'login';
require('incfiles/core.php');
require('incfiles/head.php');

use System\Core\DB as DB;

if (core::$user_id)
{
    header('Location: /');
    exit;
} else
{
    ?>

    <?
    $error = array();
    $captcha = FALSE;
    $display_form = 1;
    $user_login = isset($_POST['n']) ? functions::check($_POST['n']) : NULL;
    $user_pass = isset($_POST['p']) ? functions::check($_POST['p']) : NULL;
    $user_mem = isset($_POST['mem']) ? 1 : 0;
    $user_code = isset($_POST['code']) ? trim($_POST['code']) : NULL;
    if ($user_pass && !$user_login)
        $error[] = $lng['error_login_empty'];
    if ($user_login && !$user_pass)
        $error[] = $lng['error_empty_password'];
    if ($user_login && (mb_strlen($user_login) < 2 || mb_strlen($user_login) > 20))
        $error[] = $lng['nick'] . ': ' . $lng['error_wrong_lenght'];
    if ($user_pass && (mb_strlen($user_pass) < 3 || mb_strlen($user_pass) > 15))
        $error[] = $lng['password'] . ': ' . $lng['error_wrong_lenght'];
    if (!$error && $user_pass && $user_login)
    {
        // Запрос в базу на юзера
        $req = DB::getInstance()->query("SELECT * FROM `users` WHERE `name_lat`='" . functions::rus_lat(mb_strtolower($user_login)) . "' LIMIT 1");
        if (DB::getInstance()->numRows($req))
        {
            $user = DB::getInstance()->getAssoc($req);
            if ($user['failed_login'] > 2)
            {
                if ($user_code)
                {
                    if (mb_strlen($user_code) > 3 && $user_code == $_SESSION['code'])
                    {
                        // Если введен правильный проверочный код
                        unset($_SESSION['code']);
                        $captcha = TRUE;
                    } else
                    {
                        // Если проверочный код указан неверно
                        unset($_SESSION['code']);
                        $error[] = $lng['error_wrong_captcha'];
                    }
                } else
                {
                    // Показываем CAPTCHA
                    $display_form = 0;
                    ?>
                    <form class="form-horizontal" action="login.php<?= ($id ? '?id=' . $id : '') ?>" method="post">
                        <fieldset>
                            <legend><?= $lng['login'] ?></legend>
                            <input type="hidden" name="n" value="<?= $user_login ?>"/>
                            <input type="hidden" name="p" value="<?= $user_pass ?>"/>
                            <input type="hidden" name="mem" value="<?= $user_mem ?>"/>
                            <div class="form-group">
                                <div class="col-lg-2"></div>
                                <div class="col-lg-10">
                                    <img src="captcha.php?r=<?= rand(1000, 9999) ?>" alt="CAPTCHA">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="captcha_word" class="col-lg-2 control-label"><?= $lng['enter_code'] ?></label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" id="captcha_word" name="code" placeholder="<?= $lng['enter_code'] ?>" value="" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-10 col-lg-offset-2">
                                    <button type="submit" class="btn btn-primary" value="<?= $lng['login'] ?>"><?= $lng['login'] ?></button>
                                    <a href="/login.php" class="btn btn-default"><?= $lng['back'] ?></a>
                                </div>
                            </div>
                        </fieldset>
                    </form>

                    <?
                }
            }
            if ($user['failed_login'] < 3 || $captcha)
            {
                if (md5(md5($user_pass)) == $user['password'])
                {
                    // Если логин удачный
                    $display_form = 0;
                    DB::getInstance()->query("UPDATE `users` SET `failed_login` = '0' WHERE `id` = '" . $user['id'] . "'");
                    if (!$user['preg'])
                    {
                        // Если регистрация не подтверждена
                        echo '<div class="alert alert-dismissible alert-danger"><p>' . $lng['registration_not_approved'] . '</p></div>';
                    } else
                    {
                        // Если все проверки прошли удачно, подготавливаем вход на сайт
                        if (isset($_POST['mem']))
                        {
                            // Установка данных COOKIE
                            $cuid = base64_encode($user['id']);
                            $cups = md5($user_pass);
                            setcookie("cuid", $cuid, time() + 3600 * 24 * 365);
                            setcookie("cups", $cups, time() + 3600 * 24 * 365);
                        }
                        // Установка данных сессии
                        $_SESSION['uid'] = $user['id'];
                        $_SESSION['ups'] = md5(md5($user_pass));
                        DB::getInstance()->query("UPDATE `users` SET `sestime` = '" . time() . "' WHERE `id` = '" . $user['id'] . "'");
                        $set_user = unserialize($user['set_user']);
                        if ($user['lastdate'] < (time() - 3600) && $set_user['digest']) {
                            header('Location: ' . $set['homeurl'] . '/index.php?act=digest&last=' . $user['lastdate']);
                        } else {
                            header('Location: /');
                        }
                        exit;
                    }
                } else
                {
                    // Если логин неудачный
                    if ($user['failed_login'] < 3)
                    {
                        // Прибавляем к счетчику неудачных логинов
                        DB::getInstance()->query("UPDATE `users` SET `failed_login` = '" . ($user['failed_login'] + 1) . "' WHERE `id` = '" . $user['id'] . "'");
                    }
                    $error[] = $lng['authorisation_not_passed'];
                }
            }
        } else
        {
            $error[] = $lng['authorisation_not_passed'];
        }
    }
    if ($display_form)
    {
        if ($error)
            echo functions::display_error($error);

        $info = '';
        if (core::$system_set['site_access'] == 0 || core::$system_set['site_access'] == 1)
        {
            if (core::$system_set['site_access'] == 0)
            {
                $info = '<div class="alert alert-dismissible alert-danger">' . $lng['info_only_sv'] . '</div>';
            } elseif (core::$system_set['site_access'] == 1)
            {
                $info = '<div class="alert alert-dismissible alert-danger">' . $lng['info_only_adm'] . '</div>';
            }
        }

        echo $info;
        ?>
        <form class="form-horizontal" action="login.php" method="post">
            <fieldset>
                <legend><?= $lng['login'] ?></legend>
                <div class="form-group">
                    <label for="inputLogin" class="col-lg-2 control-label"><?= $lng['login_name'] ?></label>

                    <div class="col-lg-10">
                        <input type="text" name="n" class="form-control" id="inputLogin" value="<?= htmlentities($user_login, ENT_QUOTES, 'UTF-8') ?>" placeholder="<?= $lng['login_name'] ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword" class="col-lg-2 control-label"><?= $lng['password'] ?></label>

                    <div class="col-lg-10">
                        <input type="password" name="p" class="form-control" id="inputPassword" placeholder="<?= $lng['password'] ?>">

                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="mem" value="1" checked> <?= $lng['remember'] ?>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-2">
                        <button type="submit" class="btn btn-primary" value="<?= $lng['login'] ?>"><?= $lng['login'] ?></button>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-2">
                        <p><a href="registration.php"><?= $lng['registration'] ?></a></p>
                        <p><a href="users/skl.php?continue"><?= $lng['forgotten_password'] ?>?</a></p>
                    </div>
                </div>
            </fieldset>
        </form>

<?
    }
}

require('incfiles/end.php');